import Head from "next/head";

import { NextPageWithProvider } from "@pages/_app";
import useTranslate from "@/hooks/useTranslate";
import Dashboard from "@/features/dashboard/Dashboard";

const DashboardPage: NextPageWithProvider = () => {
  const { translate } = useTranslate();
  return (
    <>
      <Head>
        <title>{translate("dashboard.title")}</title>
      </Head>
      <Dashboard />
    </>
  );
};

export default DashboardPage;
