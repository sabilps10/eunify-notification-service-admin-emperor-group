import { useEffect } from "react";
import shallow from "zustand/shallow";
import { useRouter } from "next/router";

import filterStore, { FilterStoreTypes } from "@/store/filterStore";

const useFilter = () => {
  const {
    query: { feature }
  } = useRouter();
  const FilterStore = filterStore((state) => state, shallow) as FilterStoreTypes;

  const { filter, setFilter } = FilterStore;

  const removeFilter = () => {
    setFilter(null);
  };

  useEffect(() => {
    removeFilter();
  }, [feature]);

  return {
    filter,
    setFilter,
    removeFilter
  };
};

export default useFilter;
