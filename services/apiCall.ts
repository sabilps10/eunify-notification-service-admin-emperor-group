// import { getToken } from "@/utils/jwt-utils";
import axios, { AxiosInstance } from "axios";
import useLocalStorage from "hooks/useLocalStorage";
import useAmazonCognito from "@/hooks/useAmazonCognito";
import { LOCALSTORAGE_KEY } from "config/constants";
import toast from "react-hot-toast";

interface HeadersParams {
  data?: any;
  params?: any;
  headers?: any;
}

export enum BaseUrlVariant {
  gateway = "gateway",
}

const BASE_URL = {
  [BaseUrlVariant.gateway]: process.env.NEXT_PUBLIC_GATEWAY_URL,
};

const Api = (method: string, apiType: BaseUrlVariant, resource: string, headers?: HeadersParams) => {
  const { getItem } = useLocalStorage();
  const token = getItem(LOCALSTORAGE_KEY.TOKEN);
 
  if (token) {
    axios.defaults.headers.common = {
      Authorization: `Bearer ${token}`
    };
  }

  return axios({
    method,
    url: BASE_URL[apiType] + resource,
    ...headers
  });
};

axios.interceptors.response.use(
  res => {
    return res;
  },
  error => {
    const { getItem, removeItem } = useLocalStorage();

    const errorText = error?.response?.data?.errorDescription || "Request error, please try again";
    const toastAttr: any = {
      duration: 5000,
      position: "top-right",
      style: {
        background: "#ffe3e3",
        color: "#000000"
      },
      id: "error"
    };
    if (error?.response?.status === 401) {
      removeItem(LOCALSTORAGE_KEY.TOKEN);
      removeItem(LOCALSTORAGE_KEY.REFRESH_TOKEN);
      removeItem(LOCALSTORAGE_KEY.EXPIRED_TIME);
      setTimeout(() => {
        location.reload();
        if (window.location.origin + "/login" !== window.location.href) {
          window.location.href = "/login";
        }
      }, 1000);
    }
    if (error?.response?.status === 400) {
      toast.error("Request Error: " + errorText, toastAttr);
    }
    if (error?.response?.status === 500) {
      toast.error("Internal Server Error: " + errorText, toastAttr);
    }
    return error.response;
  }
);

const apiCall = {
  query(api_type: BaseUrlVariant, resource: string, headers?: HeadersParams) {
    return Api("GET", api_type, resource, headers);
  },
  post(api_type: BaseUrlVariant, resource: string, headers?: HeadersParams) {
    return Api("POST", api_type, resource, headers);
  },
  put(api_type: BaseUrlVariant, resource: string, headers: HeadersParams) {
    return Api("PUT", api_type, resource, headers);
  }
};

export default apiCall;
