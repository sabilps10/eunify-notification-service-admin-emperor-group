import { string } from "zod";
import apiCall, { BaseUrlVariant } from "./apiCall";

export const getReportQuery = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway, "/report/query", { data: payload });
  return res.data;
};

export const retryNotificationRequest = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway, "/request/retry", { data: payload });
  return res.data;
};

export const cancelNotificationRequest = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway, "/request/cancel", { data: payload });
  return res.data;
};

export const rescheduleNotificationRequest = async (payload: any) => {
  const res = await apiCall.post(BaseUrlVariant.gateway, "/request/reschedule", { data: payload });
  return res.data;
};
