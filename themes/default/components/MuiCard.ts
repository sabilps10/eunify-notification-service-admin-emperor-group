import { Components } from "@mui/material";

import { spacing } from "@themes/default/spacing";

export const MuiCard: Components["MuiCard"] = {
  styleOverrides: {
    root: {
      boxShadow: "none",
      borderRadius: spacing(1.5)
    }
  }
};
