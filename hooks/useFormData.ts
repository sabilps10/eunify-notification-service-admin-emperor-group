import shallow from "zustand/shallow";
import formStore, { FormStoreTypes } from "@/store/formStore";

const useFormData = () => {
  const FormStore = formStore((state) => state, shallow) as FormStoreTypes;

  const { data, setFormData } = FormStore;

  const emptyFields = () => {
    setFormData(null);
  };

  return {
    data,
    setFormData,
    emptyFields
  };
};

export default useFormData;
