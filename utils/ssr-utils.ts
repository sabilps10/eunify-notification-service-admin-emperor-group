import isUndefined from "lodash/isUndefined";

type PipedGetServerSideProps = (arg?: any) => Promise<any> | any;

export const ssrPipe =
  (...functions: PipedGetServerSideProps[]) =>
  async (
    input: any
  ): Promise<{
    props: Object;
  }> => {
    return {
      props: await functions.reduce(
        (chain, func) => chain.then(func),
        Promise.resolve(input)
      )
    };
  };

export const isSSR = () => isUndefined(typeof window);
