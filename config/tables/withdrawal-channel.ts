export const WITHDRAWAL_CHANNEL_ACTION = [{ name: "export", action: { type: "export_csv", value: "" } }];

export const WITHDRAWAL_CHANNEL_HEADER = [
  {
    id: "entityId",
    numeric: false,
    disablePadding: false,
    label: "Entity",
    isSort: true
  },
  {
    id: "gatewayNameEn",
    numeric: false,
    disablePadding: false,
    label: "Gateway",
    isSort: true
  },
  {
    id: "maxAmount",
    numeric: true,
    disablePadding: false,
    label: "Max amt",
    isSort: true
  },
  {
    id: "minAmount",
    numeric: true,
    disablePadding: false,
    label: "Min amt",
    isSort: true
  },
  {
    id: "serviceFee",
    numeric: true,
    disablePadding: false,
    label: "Service Fee"
  },
  {
    id: "status",
    numeric: false,
    disablePadding: false,
    label: "status",
    isSort: true
  },
  {
    id: "action",
    numeric: false,
    disablePadding: false,
    label: "Action",
    key_value: "gatewayNameEn",
    isSort: true,
    idValue: "id"
  }
];

export const WITHDRAW_CHANNEL_EXPORT_HEADING = [
  { name: "entityId", label: "Entity", width: 20, position: 1 },
  { name: "gatewayNameEn", label: "Gateway Name", width: 20, position: 2 },
  {
    name: "isOnlineGateway",
    type: "custom-value",
    width: 20,
    custom_values: [
      { value: true, label: "Online" },
      { value: false, label: "Offline" }
    ],
    label: "Online/Offline",
    position: 3
  },
  { name: "displayNameEn", label: "English Display Name", width: 20, position: 4 },
  { name: "displayNameTc", label: "Trad Chin Display Name", width: 30, position: 5 },
  { name: "displayNameSc", label: "Simp Chin Display Name", width: 30, position: 6 },
  {
    name: "currencies",
    type: "custom-columns-0",
    label: "Gateway Currency",
    key: "currency",
    width: 20,
    position: 7
  },
  { name: "maxAmount", label: "Max Withdrawal Amount", width: 20, position: 8 },
  { name: "minAmount", label: "Min Withdrawal Amount", width: 20, position: 9 },
  {
    name: "serviceFee",
    type: "custom-columns-0",
    key: "serviceFeeUnit",
    label: "Service Fee Rate",
    width: 20,
    position: 10
  },
  {
    name: "charges",
    type: "custom-columns",
    columns: [
      { name: "ruleKey", label: "Application Group :count", width: 30, position: 11 },
      { name: "value", label: "Group :count Amount", width: 30, position: 12 },
      { name: "unit", label: "Group :count Type", width: 30, position: 13 }
    ]
  }
];
