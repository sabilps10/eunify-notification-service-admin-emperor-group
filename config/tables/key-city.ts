export const KEY_CITY_HEADER = [
  {
    id: "regionNameEn",
    numeric: false,
    disablePadding: false,
    label: "Region/District",
    isSort: true
  },
  {
    id: "nameEn",
    numeric: false,
    disablePadding: false,
    label: "Eng Display Name",
    isSort: true
  },
  {
    id: "nameTc",
    numeric: false,
    disablePadding: false,
    label: "Trad Chi Display Name",
    isSort: true
  },
  {
    id: "nameSc",
    numeric: false,
    disablePadding: false,
    label: "Simpli Chi Display Name",
    isSort: true
  },
  {
    id: "action",
    numeric: false,
    disablePadding: false,
    label: "Action",
    key_value: "nameEn",
    idValue: "addressId"
  }
];

export const KEY_CITY_ROWS = [
  "district",
  "eng_display_name",
  "trad_chi_display_name",
  "simp_chi_display_name"
];

export const KEY_CITY_ACTION = [{ name: "add", action: { type: "link", value: "/add" } }];
