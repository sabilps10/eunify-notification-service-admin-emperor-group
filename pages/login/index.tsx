import Head from "next/head";

import { NextPageWithProvider } from "@pages/_app";
import Login from "@/features/authentication/Login";
import useTranslate from "@/hooks/useTranslate";

const LoginPage: NextPageWithProvider = () => {
  const { translate } = useTranslate();
  return (
    <>
      <Head>
        <title>{translate("auth.login.title")}</title>
      </Head>
      <Login />
    </>
  );
};

export default LoginPage;
