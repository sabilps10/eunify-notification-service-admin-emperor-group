import * as React from "react";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import { useQuery } from "react-query";

import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TablePagination from "@mui/material/TablePagination";
import TableRow from "@mui/material/TableRow";
import TableSortLabel from "@mui/material/TableSortLabel";
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle'

import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import { visuallyHidden } from "@mui/utils";

import {
  Autocomplete,
  Button,
  CircularProgress,
  Divider,
  Stack,
  TextField,
  Grid,
  Radio,
  FormControlLabel,
  IconButton,
  Checkbox,
  TextFieldProps,
} from "@mui/material";
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { MdClear as ClearIcon } from "react-icons/md";

import useTranslate from "@/hooks/useTranslate";
import useFormData from "@/hooks/useFormData";
import { makeStyles } from "@mui/styles";
import securityStore, { SecurityStoreTypes } from "@/store/securityStore";
import shallow from "zustand/shallow";
import { formatNumber, HumanizedDifference2Objects } from "@/config/utilities";
import { FilterPagination, Pagination, SortParams } from "@/types";
import useExportFile from "@/hooks/useExportFile";
import moment from "moment";

function descendingComparator(a: any, b: any, orderBy: keyof any) {
  let newA = typeof a[orderBy] === "string" ? a[orderBy]?.toLowerCase() : a[orderBy];
  let newB = typeof b[orderBy] === "string" ? b[orderBy]?.toLowerCase() : b[orderBy];

  if (newB < newA) {
    return -1;
  }
  if (newB > newA) {
    return 1;
  }
  return 0;
}

type Order = "asc" | "desc";

const useStyles = makeStyles({
  sticky: {
    position: "sticky",
    right: 0,
    background: "white"
  }
});

function getComparator<Key extends keyof any>(
  order: Order,
  orderBy: Key
): (a: { [key in Key]: any }, b: { [key in Key]: any }) => number {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort<T>(array: readonly T[], comparator: (a: T, b: T) => number) {
  const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });

  return stabilizedThis.map((el) => el[0]);
}

interface HeaderTableProps {
  rowCount: number;
  onRequestSort: (event: React.MouseEvent<unknown>, property: string) => void;
  header: any;
  onSelectAllClick: (event: React.ChangeEvent<HTMLInputElement>) => void;
  numSelected: number;
}

interface FilterTableProps {
  type: any;
  id: string;
  name: string;
  options?: any;
}

interface TableListProps {
  header: any;
  filter?: any;
  action?: any;
  callbackAction?: (name: string, value: any) => void;
  data: any;
  isLoading?: boolean;
  useCheckbox?: boolean;
  filterData?: any;
  onFilterData?: (value: any) => void;
  rowActionCallback?: (type:string, value: any) => void;
  rowActionDisabled?: boolean;
  onSelectRow?: (value: boolean, row: any, headerName?: any) => void;
  selectedData?: any[];
  exportDataObject?: any;
  actionButtonLoading?: boolean;
  isAccess?: boolean;
  isPagination?: boolean;
  pagination?: Pagination | null;
  handlePagination?: (val: FilterPagination) => void;
}

const TableList: React.FC<TableListProps> = ({
  header,
  filter = [],
  action = [],
  callbackAction = () => null,
  data = [],
  isLoading = false,
  useCheckbox = false,
  filterData,
  onFilterData = () => null,
  rowActionCallback = () => null,
  rowActionDisabled = false,
  onSelectRow,
  selectedData = [],
  exportDataObject = null,
  actionButtonLoading = false,
  isAccess = false,
  pagination = null,
  isPagination = false,
  handlePagination = () => null
}) => {
  const { client_functions } = securityStore((state) => state, shallow) as SecurityStoreTypes;
  const access = client_functions;

  const { handleSubmit, getValues, register, setValue, watch } = useForm();
  const { setFormData } = useFormData();
  const [tooltipOpen, setTooltipOpen] = React.useState<any>(null);
  const [order, setOrder] = React.useState<Order>("asc");
  const [orderBy, setOrderBy] = React.useState<string>("null");
  const [page, setPage] = React.useState(0);
  const [generatedFileHeader, setGeneratedFileHeader] = React.useState<any[]>([]);
  const [generatedFileData, setGeneratedFileData] = React.useState<any[]>([]);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const { onGenerateFile } = useExportFile();
  const [rejectData, setRejectData] = React.useState<any>(null);
  const [selected, setSelected] = React.useState<readonly string[]>([]);
  const [openReschedulePopup, setOpenReschedulePopup] = React.useState<boolean>(false)
  const [rescheduleId, setRescheduleId] = React.useState<any>(null)
  const [rescheduleDatetime, setRescheduleDatetime] = React.useState<any>(null)

  const [paginationValue, setPagination] = React.useState<FilterPagination>({
    page: 0,
    size: 10,
    sort: []
  });

  const { translate } = useTranslate();
  const classes = useStyles();
  const {
    push,
    query: { feature, tab }
  } = useRouter();

  React.useEffect(() => {
    if (data?.length && !isLoading && exportDataObject) {
      generateCsvResource();
    }
  }, [data, isLoading, exportDataObject?.data, exportDataObject?.loading]);

  React.useEffect(() => {
    if (isPagination) {
      handlePagination(paginationValue);
    }
  }, [isPagination, paginationValue]);

  const handleSelectAllClick = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.checked) {
      const newSelected: any = [];
      data.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((n:any) => {
        if(n.status.toLowerCase() == "failed"){
           newSelected.push(n.id)
        }
      }); 
      setSelected(newSelected);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event: React.MouseEvent<unknown>, id: string) => {
    const selectedIndex = selected.indexOf(id);
    let newSelected: readonly string[] = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }

    setSelected(newSelected);
  };

  const isSelected = (id: string) => selected.indexOf(id) !== -1;

  const generateCsvResource = () => {
    let cellHeading: any[] = [];
    let cellData: any[] = [];
    exportDataObject.heading.forEach(function (val: any) {
      if (val.label) {
        cellHeading.push({ col: val.label, width: val.width, name: val.name });
      }
    });

    (exportDataObject.data ? exportDataObject.data : data).forEach(function (
      data: any,
      index: number
    ) {
      let dataItem: any = {};
     
      exportDataObject.heading.forEach(function (heading: any) {
        if (heading.name !== "no" && heading.type != "custom-columns") {
          if (heading.type == "custom-value") {
            heading.custom_values.forEach(function (custom_value: any) {
              if (custom_value.value == data[heading.name])
                dataItem[heading.position.toString()] = custom_value.label;
            });
          } else if (heading.name === "gatewayCurrency") {
            dataItem[heading.position.toString()] = data?.currencies[0]?.currency;
          } else {
            dataItem[heading.position.toString()] = data[heading.name] || "N/A";
          }
        }
        if (heading.type === "custom-columns-user") {
          if (heading.name === "isActive") {
            dataItem[heading.position.toString()] = data[heading.name] === true ? "Active" : "InActive";
          }

          if (heading.name === "roleList") {
            let idx = 4;
            data[heading.name].map((value: any, dataindex: number) => {
              if (!cellHeading.find((item: any) => item.col === value.entityId)) {
                cellHeading.push({
                  col: value.entityId,
                  width: 30,
                  name: value.entityId
                });
              }
              dataItem[(idx + dataindex).toString()] = value.roleNameEn;
            });
          }
        }
        if (heading.type == "custom-columns-0") {
          if (heading.name == "currencies" && heading.key == "currency") {
            dataItem[heading.position.toString()] = data[heading.name][0]["cryptoCurrency"]
              ? data[heading.name][0]["cryptoCurrency"]
              : data[heading.name][0][heading.key];
          } else if (heading.name == "serviceFee") {
            let unit = "";
            if (data[heading.key] === "D") {
              unit = data?.currencies[0]["cryptoCurrency"]
                ? data?.currencies[0]["cryptoCurrency"]
                : data?.currencies[0]["currency"];
            } else {
              unit = "%";
            }

            dataItem[heading.position.toString()] = `${data[heading.name]} ${unit}`;
          } else {
            if (data[heading.name].length > 1) {
              let value = "";
              data[heading.name].map((el: any, index: number) => {
                value += index == 0 ? el[heading.key] : `, ${el[heading.key]}`;
              });
              dataItem[heading.position.toString()] = value;
            } else {
              dataItem[heading.position.toString()] = data[heading.name][0][heading.key];
            }
          }
        }

        if (heading.type == "custom-columns") {
          let pos = 0;
          data[heading.name].map((value: any, dataindex: number) => {
            heading.columns.map((col: any) => {
              if (!cellHeading.find((item: any) => item.col === col.label.replace(":count", dataindex + 1))) {
                cellHeading.push({
                  col: col.label.replace(":count", dataindex + 1),
                  width: col.width,
                  name: col.name
                });
              }
              if (col.name == "unit") {
                dataItem[col.position + dataindex * heading.columns.length] =
                  value[col.name].toLowerCase() === "p"
                    ? "%"
                    : data["currencies"][0]["cryptoCurrency"]
                    ? data["currencies"][0]["cryptoCurrency"]
                    : data["currencies"][0]["currency"];
              } else {
                dataItem[col.position + dataindex * heading.columns.length] = value[col.name];
              }
            });
          });
        }

        if (heading.type == "custom-data") {
          const isDeposit = data["tradeType"].toLowerCase().includes("mi");
          const isWithdraw = data["tradeType"].toLowerCase().includes("mo");

          if (heading.key == "exchange-rate") {
            if (data["actExchangeRate1"] && data["actExchangeRate2"]) {
              dataItem[heading.position.toString()] = (
                data["actExchangeRate1"] * data["actExchangeRate2"]
              ).toString();
            } else {
              dataItem[heading.position.toString()] = (
                data["tmpExchangeRate1"] * data["tmpExchangeRate2"]
              ).toString();
            }
          }

          if (heading.key == "actual-amount") {
            if (isDeposit) {
              if (data["actAmount"]) {
                dataItem[heading.position.toString()] = data["actAmount"];
              } else {
                dataItem[heading.position.toString()] = data["tmpAmount"];
              }
            }
            if (isWithdraw) {
              if (data["actDepositAmount"]) {
                dataItem[heading.position.toString()] = data["actDepositAmount"];
              } else {
                dataItem[heading.position.toString()] = data["tmpDepositAmount"];
              }
            }
          }

          if (heading.key == "actual-currency") {
            if (isDeposit) {
              dataItem[heading.position.toString()] = data["currency"];
            }
            if (isWithdraw) {
              dataItem[heading.position.toString()] = data["settleCurrency"];
            }
          }

          if (heading.key == "platform-currency") {
            dataItem[heading.position.toString()] = "USD";
          }

          if (heading.key == "platform-amount") {
            if (isDeposit) {
              dataItem[heading.position.toString()] = data["actDepositAmount"]
                ? data["actDepositAmount"]
                : data["tmpDepositAmount"];
            }
            if (isWithdraw) {
              dataItem[heading.position.toString()] = data["actAmount"]
                ? data["actAmount"]
                : data["tmpAmount"];
            }
          }

          if (heading.key == "service-charge") {
            dataItem[heading.position.toString()] = data["actCharge"] ? data["actCharge"] : data["tmpCharge"];
          }

          if (heading.key == "calculated-actual-deposit") {
            let actualAmount = 0;
            const serviceCharge = data["actCharge"] ? data["actCharge"] : data["tmpCharge"];
            if (isWithdraw) {
              actualAmount = data["actDepositAmount"] ? data["actDepositAmount"] : data["tmpDepositAmount"];
            }
            if (isDeposit) {
              actualAmount = data["actAmount"] ? data["actAmount"] : data["tmpAmount"];
            }
            if (data["tradeType"].toLowerCase() == "mi" || data["tradeType"].toLowerCase() == "mia") {
              dataItem[heading.position.toString()] = actualAmount - serviceCharge;
            } else {
              dataItem[heading.position.toString()] = "N/A";
            }
          }

          if (heading.key == "platform-currency") {
            dataItem[heading.position.toString()] = "USD";
          }
          if (heading.key == "bank-location") {
            dataItem[heading.position.toString()] = data.bankLocation?.nameEn || "N/A";
          }
          if (heading.key == "bank-province") {
            dataItem[heading.position.toString()] = data.bankProvince?.nameEn || "N/A";
          }
          if (heading.key == "bank-region") {
            dataItem[heading.position.toString()] = data.bankRegion?.nameEn || "N/A";
          }
          if (heading.key == "bank-city") {
            dataItem[heading.position.toString()] = data.bankCity?.nameEn || "N/A";
          }
        }
      });
      cellData.push(dataItem);
    });
    setGeneratedFileHeader(cellHeading);
    setGeneratedFileData(cellData);
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const emptyRows =
    page > 0
      ? Math.max(0, (1 + page) * rowsPerPage - (access ? data.length : data.length))
      : 0;

  const handleSearch = () => {
    const queryFilter = getValues();
    setPage(0);
    setPagination({ ...paginationValue, page: 0 });
    onFilterData(queryFilter);
  };

  function RenderTableHead(props: HeaderTableProps) {
    const { header, onRequestSort, onSelectAllClick, numSelected, rowCount} = props;

    const createSortHandler = (property: any) => (event: React.MouseEvent<unknown>) => {
      onRequestSort(event, property);
    };

    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            <Checkbox
              disabled={isLoading}
              color="primary"
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={rowCount > 0 && numSelected === rowCount}
              onChange={onSelectAllClick}
              inputProps={{
                'aria-label': 'select all desserts',
              }}
            />
          </TableCell>
          {header.map((headCell: any, key: number) => {
            let id = headCell?.sortId ? headCell?.sortId : headCell.id;

            const hasSort = headCell.isSort;
            const isActive = paginationValue.sort.find((e: SortParams) => e.field === id) as SortParams;

            let direction: any = "asc" || undefined;
            let sortDirection: any = "asc";
            let active: boolean = false;

            if (!isPagination) {
              sortDirection = orderBy === id ? "desc" : "asc" || "asc";
              active = orderBy === id;
              direction = orderBy === id ? order : "asc" || "asc";
            } else {
              direction = isActive?.ascOrder ? "desc" : "asc" || "asc";
              sortDirection = isActive?.ascOrder ? "desc" : "asc" || "asc";
              active = isActive?.field === id;
            }

            return (
              <TableCell
                key={key}
                align="center"
                sortDirection={hasSort ? sortDirection : undefined}
                className={headCell.sticky ? classes.sticky : ""}
              >
                <TableSortLabel
                  sx={{
                    "&.MuiTableSortLabel-root": {
                      cursor: hasSort ? "pointer" : "unset",
                      "&:hover": {
                        cursor: hasSort ? "pointer" : "unset",
                        color: "unset",
                        ">.MuiSvgIcon-root": {
                          visibility: hasSort ? "initial" : "hidden"
                        }
                      }
                    }
                  }}
                  active={hasSort ? active : false}
                  direction={hasSort ? direction : undefined}
                  onClick={hasSort ? createSortHandler(id) : undefined}
                >
                  <Typography variant="body" fontWeight={700}>
                    {headCell.label}
                  </Typography>
                  {orderBy === id ? (
                    <Box component="span" sx={visuallyHidden}>
                      {order === "desc" ? "sorted descending" : "sorted ascending"}
                    </Box>
                  ) : null}
                </TableSortLabel>
              </TableCell>
            );
          })}
        </TableRow>
      </TableHead>
    );
  }

  const RenderFilterOption: React.FC<FilterTableProps> = ({ id, name, type, options }) => {
    /* note: add every possibly type of filter here */
    let defaultValue = "";

    if (filterData) {
      if (typeof filterData[name] == "boolean") {
        defaultValue = filterData[name].toString();
      } else if (typeof filterData[name] == "number") {
        defaultValue = getValues(name);
      } else if (filterData[name]) {
        defaultValue = filterData[name].toString();
      } else {
        defaultValue = getValues(name) || "";
      }
    } else {
      defaultValue =
        typeof getValues(name) === "boolean" || getValues(name) === 0 || getValues(name)
          ? getValues(name)
          : "";
    }

    switch (type) {
      case "radio":
        return (
          <Stack direction="column" gap={1}>
            <Typography variant="body">{translate(`dashboard.labels.${id}`)}</Typography>
            <Stack direction="row">
              {options &&
                options?.map((option: any, key: number) => (
                  <FormControlLabel key={key} value={option} control={<Radio />} label={option} />
                ))}
            </Stack>
          </Stack>
        );
      case "dropdown":
        return (
          <Stack direction="column" gap={1}>
            <Typography variant="body">{translate(`dashboard.labels.${id}`)}</Typography>
            <Autocomplete
              {...register(name)}
              disablePortal
              id="entity"
              size="small"
              defaultValue={options?.find((v: any) => v.value === defaultValue) || null}
              options={options}
              sx={{ width: "100%" }}
              onChange={(_, value: any) => setValue(name, value?.value)}
              renderInput={(params) => <TextField {...params} inputProps={{
                ...params.inputProps,
                onKeyPress: (e) => {
                  if(e.key.match(/[`#$%^&*"_+=\[\]{};':"\\|<>\/?~]/g)){
                    e.preventDefault();
                  }
                },
              }}/>}
            />
          </Stack>
        );
      case "time-range":
        return (
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <Stack direction="column" gap={1}>
              <Typography variant="body">{translate(`dashboard.labels.${id}`)}</Typography>
              <Stack direction="row" alignItems="center" gap={2}>
                <Stack direction="row" alignItems="center" gap={1}>
                  <DatePicker
                    inputFormat="yyyy-MM-dd"
                    views={["year", "day"]}
                    value={watch(`${name}_start`) || moment()}
                    onAccept={(value) => {
                      setValue(`${name}_start`, value);
                    }}
                    onChange={() => null}
                  
                    renderInput={(params) => (
                      <TextField
                        size="small"
                        {...params}
                        inputProps={{
                          ...params.inputProps,
                          placeholder: "Select Date Time"
                        }}
                      />
                    )}
                  />
                  {watch(`${name}_start`) && (
                    <IconButton>
                      <ClearIcon
                        style={{ cursor: "pointer" }}
                        onClick={() => {
                          setValue(`${name}_start`, null);
                        }}
                      />
                    </IconButton>
                  )}
                </Stack>
                <Stack direction="row" alignItems="center" gap={1}>
                  <DatePicker
                    inputFormat="yyyy-MM-dd"
                    views={["year", "day"]}
                    value={watch(`${name}_end`) || moment()}
                    onAccept={(value) => {
                      setValue(`${name}_end`, value);
                    }}
                    onChange={() => null}
                    renderInput={(params) => (
                      <TextField
                        size="small"
                        {...params}
                        inputProps={{
                          ...params.inputProps,
                          placeholder: "Select Date Time"
                        }}
                      />
                    )}
                  />
                  {watch(`${name}_end`) && (
                    <IconButton>
                      <ClearIcon
                        style={{ cursor: "pointer" }}
                        onClick={() => setValue(`${name}_end`, null)}
                      />
                    </IconButton>
                  )}
                </Stack>
              </Stack>
            </Stack>
          </LocalizationProvider>
        );
      default:
        return (
          <Stack direction="column" gap={1}>
            <Typography variant="body">{translate(`dashboard.labels.${id}`)}</Typography>
            <TextField
              {...register(name)}
              size="small"
              onChange={(e) => {
                let val = e.target.value.replaceAll(/[`#$%^&*"_+=\[\]{};':"\\|<>\/?~]/g, "");
                setValue(name, val);
              }}
              sx={{ width: "100%" }}
            />
          </Stack>
        );
    }

    /* end of note */
    return <></>;
  };

  const TableToolbar = () => {
    const [currencies, setCurrencies] = React.useState<any>({});

    React.useEffect(() => {
      if (selectedData.length > 0) {
        selectedData.map((el: any) => {
          if (currencies[el.currency]) {
            setCurrencies({
              ...currencies,
              [el.currency]: el.actAmount + currencies[el.currency]
            });
          } else {
            setCurrencies({ ...currencies, [el.currency]: el.actAmount });
          }
        });
      }
    }, [selectedData]);

    const handleTableAction = (action: any) => {
      if (action.type == "link") {
        push(`/${feature}${action.value}`);
      }
      if (action.type == "callback_action") {
        callbackAction(action.value, selectedData);
      }
      if (action.type == "export_csv") {
        if (
          (data || exportDataObject.data.length) &&
          (data.length || exportDataObject.data.length)
        ) {
          onGenerateFile(generatedFileData, generatedFileHeader, feature + "_" + new Date(), ".xlsx");
        } else {
          alert("Data not ready yet!");
        }
      }
      if(action == "retry-notification"){
        setSelected([]);
        rowActionCallback("retry", selected);
      }
    };

    return (
      <Stack>
        <Stack sx={{ width: "100%" }}>
          <Stack gap={3} direction="row" justifyContent="space-between">
            <Grid container spacing={2} padding={2}>
              {filter.map((value: any, key: number) => {
                return (
                  <Grid item md={value.type === "time-range" ? 6 : 4} key={key}>
                    <RenderFilterOption
                      type={value.type}
                      id={value.id}
                      name={value.name}
                      options={value.options}
                    />
                  </Grid>
                );
              })}
            </Grid>
          </Stack>
          <Divider />
          <Stack alignItems="flex-end">
            <Stack direction="row" alignItems="flex-end" gap={2} padding={2}>
              {filter.length ? (
                <Button size="small" sx={{ height: 35 }} onClick={handleSearch}>
                  <Typography variant="body" fontWeight={700}>
                    {translate("dashboard.buttons.search")}
                  </Typography>
                </Button>
              ) : null}

              {action &&
                action.map((value: any, key: any) => {
                  if (value.name == "export" && !data.length) {
                    return null;
                  }

                  if(value.name == "retry-selected-notification"){
                    return (<Stack key={key}>
                      <Button
                        onClick={() => handleTableAction(value.action)}
                        size="small"
                        variant="outlined"
                        sx={{ height: 35 }}
                        disabled={selected.length == 0}
                      >
                        <Typography variant="body" fontWeight={700}>
                          {translate(`dashboard.buttons.${value.name}`)}
                        </Typography>
                      </Button>
                    </Stack>)
                  }
                  return (
                    <Stack key={key}>
                      <Button
                        onClick={() => handleTableAction(value.action)}
                        size="small"
                        variant="outlined"
                        sx={{ height: 35 }}
                        disabled={
                          value.name != "export"
                            ? false
                            : exportDataObject.loading
                            ? exportDataObject.loading
                            : false
                        }
                      >
                        <Typography variant="body" fontWeight={700}>
                          {translate(`dashboard.buttons.${value.name}`)}
                        </Typography>
                      </Button>
                    </Stack>
                  );
                })}
            </Stack>
          </Stack>
        </Stack>
      </Stack>
    );
  };

  const RenderRowAction = ({
    feature,
    row,
    action_key,
  }: {
    feature: any;
    row: any;
    action_key: any;
  }) => {
    /* note: add every possibly feature action here */
    switch (feature) {
      case "report-management":
        return  (
          <Stack direction="row" gap={1}>
            <Button
              sx={{ height: 45 }}
              onClick={function () {
                setFormData(row);
                push({ pathname: `/${feature}/detail`, query: { id: action_key } });
              }}
            >
              <Typography variant="body" fontWeight="bold">
                {translate("dashboard.buttons.details")}
              </Typography>
            </Button>
            {row.status.toLowerCase() == "failed" && (
              <Button
              sx={{ height: 45 }}
              disabled={rowActionDisabled}
              onClick={() => rowActionCallback("retry", [row.id])}
              >
                <Typography variant="body" fontWeight="bold">
                  {translate("dashboard.buttons.retry")}
                </Typography>
              </Button>
            )}
            {row.scheduleType.toLowerCase() == "scheduled" && row.status.toLowerCase() == "pending" && (
              <Button
              sx={{ height: 45 }}
              disabled={rowActionDisabled}
              onClick={() => rowActionCallback("cancel", [row.id])}
              >
                <Typography variant="body" fontWeight="bold">
                  {translate("dashboard.buttons.cancel")}
                </Typography>
              </Button>
            )}
            {row.scheduleType.toLowerCase() == "scheduled" && row.status.toLowerCase() == "pending" && (
              <>
              <Button
              sx={{ height: 45 }}
              disabled={rowActionDisabled}
              onClick={() => {
                setOpenReschedulePopup(true)
                setRescheduleId(row.id)
              }}
              >
                <Typography variant="body" fontWeight="bold">
                  {translate("dashboard.buttons.reschedule")}
                </Typography>
              </Button>
              
              </>
            )}
            
          </Stack>
        );
      default:
        return (
          <Button
            sx={{ height: 45 }}
            onClick={function () {
              setFormData(row);
              push({ pathname: `/${feature}/detail`, query: { id: action_key } });
            }}
          >
            <Typography variant="body" fontWeight="bold">
              {translate("dashboard.buttons.edit")}
            </Typography>
          </Button>
        );
    }
    /* end of note */
  };

  const RenderRowData = ({ row, index }: { row: any; index: number }) => {

    return (
      <>
        {header.map((val: any, key: number) => {
          let rowValue = typeof row[val.id] == "boolean" ? row[val.id].toString() : row[val.id];
          let show = true;
          if(val.id == "toRecipients"){
            return (<TableCell style={{overflowWrap: 'anywhere'}} align="center" key={Math.random()} sx={{ color: row['emailContent']['toRecipients'] ? "#111" : "#a1a1a1" }}>
              {show ? (row['emailContent']['toRecipients'].toString() ? row['emailContent']['toRecipients'].toString() : "N/A") : "N/A"}
            </TableCell>)
          }
          if(val.id == "retryCount"){
            return (<TableCell align="center" key={Math.random()} sx={{ color: "#111"}}>
              {show ? (row['retryCount'].toString() ? row['retryCount'].toString() : 0) : 0}
            </TableCell>)
          }
          if (val.id == "action") {
            return (
              <TableCell align="center" key={Math.random()} className={val.sticky ? classes.sticky : ""}>
                <RenderRowAction
                  feature={feature}
                  row={row}
                  action_key={row[val.key_value]}
                />
              </TableCell>
            );
          }
          return (
            <TableCell align="center" key={Math.random()} sx={{ color: rowValue ? "#111" : "#a1a1a1" }}>
              {show ? (rowValue ? rowValue : "N/A") : "N/A"}
            </TableCell>
          );
        })}
      </>
    );
  };

  const handleRequestSort = (event: React.MouseEvent<unknown>, property: string) => {
    const isAsc = orderBy === property && order === "asc";

    let sortList = [...paginationValue.sort];
    const sortIndex = sortList.findIndex((e) => e.field === property);

    if (isPagination) {
      if (sortIndex === -1) {
        sortList = [{ field: property, ascOrder: isAsc }];
      } else {
        sortList[sortIndex].ascOrder = !sortList[sortIndex].ascOrder;
      }

      setPagination({
        ...paginationValue,
        sort: sortList
      });
    }

    setOrder(isAsc ? "desc" : "asc");
    if (!isPagination) {
      setOrderBy(property);
    }
  };
  return (
    <Box sx={{ width: "100%" }}>
      <Paper sx={{ width: "100%", mb: 2 }}>
        <TableToolbar />
        <Divider />
        <TableContainer>
          <Table>
            <RenderTableHead 
              onSelectAllClick={handleSelectAllClick} 
              numSelected={selected.length}
              header={header} 
              rowCount={data?.length} 
              onRequestSort={handleRequestSort} 
            />
            <TableBody>
              {isLoading && (
                <TableRow>
                  <TableCell colSpan={header.length + 1} align="center">
                    <CircularProgress color="inherit" size={25} />
                  </TableCell>
                </TableRow>
              )}
              {!isLoading
                ? isPagination
                  ? stableSort(isAccess ? data : data, getComparator(order, orderBy)).map(
                      (row: any, index: number) => {
                        const isItemSelected = isSelected(row.id);
                        const labelId = `table-checkbox-${index}`;
                        return (
                          <TableRow tabIndex={-1} key={index} hover>
                            <TableCell padding="checkbox">
                              {row.status.toLowerCase() == "failed" && (
                                <Checkbox
                                  onClick={(event) => handleClick(event, row.id)} role="checkbox"
                                  color="primary"
                                  checked={isItemSelected}
                                  inputProps={{
                                    'aria-labelledby': labelId,
                                  }}
                                />
                              )}
                            </TableCell>
                            <RenderRowData key={index} row={row} index={index} />
                          </TableRow>
                        );
                      }
                    )
                  : stableSort(isAccess ? data : data, getComparator(order, orderBy))
                      .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                      .map((row: any, index: number) => {
                        const isItemSelected = isSelected(row.id);
                        const labelId = `table-checkbox-${index}`;
                        return (
                          <TableRow tabIndex={-1} key={index} hover>
                            <TableCell padding="checkbox">
                              {row.status.toLowerCase() == "failed" && (
                                <Checkbox
                                  onClick={(event) => handleClick(event, row.id)} role="checkbox"
                                  color="primary"
                                  checked={isItemSelected}
                                  inputProps={{
                                    'aria-labelledby': labelId,
                                  }}
                                />
                              )}
                              
                            </TableCell>
                            <RenderRowData key={index} row={row} index={index} />
                          </TableRow>
                        );
                      })
                : null}
              {(isAccess ? !data?.length : !data.length) && !isLoading && (
                <TableRow>
                  <TableCell colSpan={header.length + 1} align="center" sx={{ background: "#ededed" }}>
                    No Data Available
                  </TableCell>
                </TableRow>
              )}
              {emptyRows > 0 && (
                <TableRow>
                  <TableCell colSpan={header.length + 1} align="center" sx={{ background: "#ededed" }}>
                    End of row
                  </TableCell>
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        {!isPagination && data?.length ? (
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={isAccess ? data.length : data.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={handleChangePage}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        ) : null}
        {!isLoading && isPagination && pagination ? (
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={pagination?.totalElements}
            rowsPerPage={paginationValue?.size}
            page={paginationValue.page}
            onPageChange={(val, newVal) => {
              setPagination({ ...paginationValue, page: newVal });
            }}
            onRowsPerPageChange={(event) => {
              setPagination({ ...paginationValue, size: parseInt(event.target.value), page: 0 });
            }}
          />
        ) : null}
      </Paper>
      <Dialog open={openReschedulePopup} onClose={() => setOpenReschedulePopup(false)} >
      <DialogTitle>Reschedule</DialogTitle>
      <DialogContent>
        <LocalizationProvider dateAdapter={AdapterDateFns}>
        <Stack>
          <DateTimePicker
            renderInput={(props) => <TextField {...props} />}
            value={rescheduleDatetime}
            inputFormat="yyyy-MM-dd hh:mm:ss"
            onAccept={(newValue) => {
              console.log(newValue)
              setRescheduleDatetime(newValue);
            }}
            onChange={() => null}
          />
        </Stack>
        </LocalizationProvider>
        <Stack direction="row" spacing={2} marginTop={2}>
          <Button sx={{ height: 45, fontWeight: 'bold' }} onClick={() => {
            setRescheduleId(null)
            setRescheduleDatetime(null)
            setOpenReschedulePopup(false)
          }}>Cancel</Button>
          <Button sx={{ height: 45, fontWeight: 'bold' }} disabled={!rescheduleDatetime} onClick={() => {
            rowActionCallback("reschedule", {id: rescheduleId, datetime: rescheduleDatetime})
            setRescheduleId(null)
            setRescheduleDatetime(null)
            setOpenReschedulePopup(false)
          }}>Reschedule</Button>
        </Stack>
      </DialogContent>
      <DialogActions>
        
      </DialogActions>
    </Dialog>
    </Box>
  );
};

export default TableList;
