export const KEY_PROVINCE_HEADER = [
  {
    id: "nameEn",
    numeric: false,
    disablePadding: false,
    label: "Eng Display Name",
    isSort: true
  },
  {
    id: "nameSc",
    numeric: false,
    disablePadding: false,
    label: "Simpli Chi Display Name",
    isSort: true
  },
  {
    id: "nameTc",
    numeric: false,
    disablePadding: false,
    label: "Trad Chi Display Name",
    isSort: true
  },
  {
    id: "action",
    numeric: false,
    disablePadding: false,
    label: "Action",
    key_value: "nameEn",
    isSort: true,
    idValue: "addressId"
  }
];

export const KEY_PROVINCE_ROWS = ["eng_display_name", "trad_chi_display_name", "simp_chi_display_name"];

export const KEY_PROVINCE_ACTION = [{ name: "add", action: { type: "link", value: "/add" } }];
