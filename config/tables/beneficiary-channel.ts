export const BENEFICIARY_ACCOUNT_HEADER = [
  {
    id: "no",
    numeric: false,
    disablePadding: false,
    label: "No.",
    isSort: true
  },
  {
    id: "entityName",
    numeric: false,
    disablePadding: false,
    label: "Entity",
    isSort: true
  },
  {
    id: "paymentGatewayName",
    numeric: false,
    disablePadding: false,
    label: "Gateway Label",
    isSort: true
  },
  {
    id: "action",
    numeric: false,
    disablePadding: false,
    label: "Action",
    key_value: "paymentGatewayName",
    idValue: "bankId"
  }
];
