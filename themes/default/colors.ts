import { createTheme, PaletteOptions } from "@mui/material/styles";

declare module "@mui/material/styles" {
  export interface PaletteOptions {
    brandBlue: {
      500: string;
      800: string;
    };

    brandYellow: {
      200: string;
      300: string;
      500: string;
      800: string;
    };

    brandGrey: {
      300: string;
      500: string;
      800: string;
    };

    brandRed: {
      500: string;
    };

    neutrals: {
      text: string;
      white: string;
      black: string;
    };

    misc: {
      green: string;
    };
  }

  interface Palette {
    brandBlue: PaletteOptions["brandBlue"];
    brandYellow: PaletteOptions["brandYellow"];
    brandGrey: PaletteOptions["brandGrey"];
    brandRed: PaletteOptions["brandRed"];
    neutrals: PaletteOptions["neutrals"];
    misc: PaletteOptions["misc"];
  }
}

const newPalette: PaletteOptions = {
  brandBlue: {
    500: "hsla(189, 100%, 41%, 1)",
    800: "hsla(199, 100%, 46%, 1)"
  },
  brandYellow: {
    200: "hsla(34, 49%, 63%, 1)",
    300: "hsla(45, 99%, 69%, 0.3)",
    500: "hsla(45, 99%, 69%, 1)",
    800: "hsla(35, 49%, 63%, 1)"
  },
  brandGrey: {
    300: "hsla(0, 0%, 50%, 1)",
    500: "hsla(0, 0%, 93%, 1)",
    800: "hsla(0, 0%, 62%, 1)"
  },
  brandRed: {
    500: "hsla(0, 100%, 62%, 1)"
  },
  neutrals: {
    black: "#111",
    text: "hsla(0, 0%, 27%, 1)",
    white: "#fff"
  },
  misc: {
    green: "00CB00"
  }
};

const colorTheme = createTheme({ palette: newPalette });

export const { palette } = colorTheme;

export default colorTheme;
