import { FC, PropsWithChildren, useState } from "react";
import { useRouter } from "next/router";

import {
  FormControl,
  FormLabel,
  Typography,
  TextField,
  MenuItem,
  TextFieldProps,
  Autocomplete
} from "@mui/material";
import { Controller, useFormContext } from "react-hook-form";

export type InputSelectProps = {
  label?: string;
  fullWidth?: boolean;
  required?: boolean;
  name: string;
  options: any;
  sx?: any;
  size?: string;
  defaultValue?: any;
  addQuery?: boolean;
  set?: any;
  onSelectCallback?: (val: any) => void;
  hasSelectCallback?: boolean;
  selectedOptionValue?: string;
  autocomplete?: boolean;
  get?: any;
  watch?: any;
} & TextFieldProps;

const InputSelect: FC<InputSelectProps> = ({
  label,
  fullWidth,
  required,
  name,
  sx,
  options,
  size,
  defaultValue,
  addQuery = false,
  set,
  watch,
  get,
  onSelectCallback,
  hasSelectCallback = false,
  selectedOptionValue,
  autocomplete,
  ...props
}) => {
  const {
    control,
    formState: { errors }
  } = useFormContext();
  const { query, push } = useRouter();

  const handleSelect = (val: any) => {
    let value = val;
    if (selectedOptionValue) {
      value = options.find((obj: any) => obj.origin[selectedOptionValue] == val).origin;
    }
    if (hasSelectCallback && onSelectCallback) {
      onSelectCallback(value);
    } else {
      set(name, value, { shouldValidate: true });
    }

    if (addQuery) {
      push({ pathname: `/${query.feature}/${query.page}`, query: { ...query, [name]: val } });
    }
    return;
  };

  const error = errors[name] ? errors[name]?.message : "";

  return (
    <Controller
      control={control}
      name={name}
      defaultValue={defaultValue}
      render={({ field }) => {
        let selected = -1;
        if (autocomplete && options.length) {
          selected = options.findIndex((v: any) => v.value == field.value);
        }
        return (
          <>
            <FormControl sx={{ gap: 1 }} fullWidth required={required} error={!!errors[name]}>
              {label && <FormLabel color="secondary">{label}</FormLabel>}
              {autocomplete ? (
                <Autocomplete
                  value={selected > -1 ? options[selected] : { label: "", value: "" }}
                  key={"autocomplete-" + field.name}
                  options={options}
                  getOptionLabel={(option: any) => option.label}
                  isOptionEqualToValue={(option, value) =>
                    value === undefined || value === "" || option.value === value.value
                  }
                  noOptionsText={"No Option Available"}
                  onChange={(_, data: any) => {
                    handleSelect(data?.value || "");
                  }}
                  renderOption={(props, option: any) => {
                    return (
                      <Typography variant="body" {...props} key={option.value}>
                        {option.label}
                      </Typography>
                    );
                  }}
                  renderInput={(params) => {
                    return <TextField {...params} placeholder={"Please Select"} variant="outlined" inputProps={{
                      ...params.inputProps,
                      onKeyPress: (e) => {
                        if(e.key.match(/[`#$%^&*"_+=\[\]{};':"\\|<>\/?~]/g)){
                          e.preventDefault();
                        }},
                                          }}/>;
                  }}
                  sx={{
                    border: "solid",
                    borderWidth: !!errors[name] ? 1 : 0,
                    borderColor: "red",
                    borderRadius: 1,
                    ...sx
                  }}
                />
              ) : (
                <TextField
                  select
                  {...props}
                  {...field}
                  value={field.value || ""}
                  sx={{
                    border: "solid",
                    borderWidth: !!errors[name] ? 1 : 0,
                    borderColor: "red",
                    ...sx
                  }}
                  fullWidth={fullWidth}
                  required={required}
                  size={size}
                  onChange={(event) => handleSelect(event.target.value)}
                >
                  {options.map((value: any, key: number) => {
                    return (
                      <MenuItem key={key} value={value.value}>
                        {value.label}
                      </MenuItem>
                    );
                  })}
                </TextField>
              )}

              <Typography variant="body" color="brandRed.500">{`${error}`}</Typography>
            </FormControl>
          </>
        );
      }}
    ></Controller>
  );
};

export default InputSelect;
