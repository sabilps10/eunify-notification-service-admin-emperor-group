import type { FC, PropsWithChildren } from "react";

import PageLayout, { PageLayoutProps } from "./PageLayout";
import SubPageLayout, { SubPageLayoutProps } from "./SubPageLayout";

type LayoutVariant = "page" | "subpage";

export type LayoutProps = PropsWithChildren<
  {
    variant?: LayoutVariant;
  } & (PageLayoutProps | SubPageLayoutProps)
>;

const Layout: FC<LayoutProps> = ({ variant, ...props }) => {
  switch (variant) {
    case "page":
      return <PageLayout {...(props as PageLayoutProps)} />;

    case "subpage":
      return <SubPageLayout {...(props as SubPageLayoutProps)} />;

    default:
      return null;
  }
};

export default Layout;
