import { FC } from "react";

import { Stack } from "@mui/material";
import { useRouter } from "next/router";
import useTranslate from "@/hooks/useTranslate";
import ReportListing from "./modules/reports/Listing";
import ReportForm from "./modules/reports/Form";

const RenderContent: FC = () => {
  const {
    query: { page, feature }
  } = useRouter();
  const { translate } = useTranslate();
  if (feature == "report-management") {
    if (page == "management") {
      return <ReportListing />;
    }
    if(page == "detail"){
      return <ReportForm />;
    }
  }
  return <></>;
};

const DashboardContent: FC = () => {
  return (
    <Stack sx={{ mt: 3 }}>
      <RenderContent />
    </Stack>
  );
};

export default DashboardContent;
