import { FC, useCallback, useEffect, useState } from "react";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import { EditorProps } from "react-draft-wysiwyg";
import "../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

import {
  Typography,
  Stack,
  Card,
  CardContent,
  Box,
  Divider,
  Grid,
  Button,
  FormLabel,
  TextField,
  FormControl
} from "@mui/material";
import useTranslate from "@/hooks/useTranslate";
import { FormProvider, useForm, SubmitHandler } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { TypeOf } from "zod";
import InputText from "@/components/FormFields/InputText";
import InputSelect from "@/components/FormFields/InputSelect";
import InputCheckbox from "@/components/FormFields/InputCheckbox";
import InputFile from "@/components/FormFields/InputFile";
import useFormContent from "../hooks/useFormContent";
import InputRadio from "@/components/FormFields/InputRadio";
import { palette } from "@/themes/default/colors";
import { ContentState, convertFromHTML, convertToRaw, EditorState } from "draft-js";
import draftToHtml from "draftjs-to-html";
import securityStore, { SecurityStoreTypes } from "@/store/securityStore";
import shallow from "zustand/shallow";
import convertNameToUrl from "@/utils/convertToPathUrl";
import toast from "react-hot-toast";

import InputDate from "@/components/FormFields/InputDate";
import InputTime from "@/components/FormFields/InputTime";
import useLocalStorage from "@/hooks/useLocalStorage";
import { LOCALSTORAGE_KEY } from "@/config/constants";

import InputDateTime from "@/components/FormFields/InputDateTime";
import RejectModal from "./RejectModal";

const Editor = dynamic<EditorProps>(() => import("react-draft-wysiwyg").then((mod) => mod.Editor), {
  ssr: false
});

const RenderForm = ({
  field,
  methods,
  defaultValue,
  formData,
  setError,
  parentCallback,
  onSelectCallback,
  getAmlStatus
}: {
  field: any;
  methods: any;
  defaultValue: any;
  formData: any;
  setError: any;
  parentCallback: (func: any) => void;
  onSelectCallback: (func: any, value: any) => void;
  getAmlStatus?: (t: any) => void | null;
}) => {
  const [, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);
  const [formValues, setFormValues] = useState(null);
  const { translate } = useTranslate();
  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const onEditorStateChange = (editorState: any) => {
    methods.setValue(field.name, draftToHtml(convertToRaw(editorState.getCurrentContent())));
    setEditorState(editorState);
  };

  useEffect(() => {
    const subscription = methods.watch((value: any) => {
      setFormValues(value);
    });
    return () => subscription.unsubscribe();
  }, [methods.watch]);

  useEffect(() => {
    getAmlStatus?.(methods.getValues("aml_status"));
  }, [methods.getValues("aml_status")]);

  useEffect(() => {
    if (defaultValue) {
      methods.setValue(field.name, defaultValue);

      if (field.name === "content" && field.type === "editor") {
        const blocks = convertFromHTML(defaultValue);
        const contentState = ContentState.createFromBlockArray(blocks.contentBlocks);
        const editorState = EditorState.createWithContent(contentState);

        setEditorState(editorState);
      }
    }
  }, [defaultValue]);

  useEffect(() => {
    if (field.name == "city") {
      console.log(field);
    }
    forceUpdate();
  }, [field.hidden]);

  if (field.callback) {
    const callback = useCallback(() => {
      parentCallback(field.callback.function);
    }, []);

    useEffect(() => {
      const enabledValues = methods.getValues(field.callback.enabled);
      const enabled = enabledValues.indexOf("") === -1 && enabledValues.indexOf(undefined) === -1;
      if (enabled) {
        callback();
      }
    }, [JSON.stringify(methods.getValues(field.callback.enabled))]);
  }

  let show = true;
  let disabled = field.disabled;

  if (field.defaultValue) {
    defaultValue = field.defaultValue;
  }

  /* IF FIELD HAS VALUE WITH MULTIPLE VALUE TO SHOW */
  if (field?.ids?.length) {
    let fieldValue = "";
    field.ids.forEach((id: any) => {
      if (id.data == "static") {
        fieldValue += id.value + " ";
      } else {
        if (eval(id.data)) {
          fieldValue += eval(id.data)[id.value] + " ";
        }
      }
    });
    defaultValue = fieldValue;
  }

  /*
    IF FIELD HAS AFFECTED ACTION TO EXECUTE.
    (EXAMPLE: HIDE OR SHOWING THE FIELD)
  */
  if (
    field.affected &&
    formValues &&
    formValues[field.affected.field] &&
    (typeof formValues[field.affected.field] === "string"
      ? field.affected.value == formValues[field.affected.field]
      : field.affected.value == formValues[field.affected.field][0])
  ) {
    if (field.affected.action == "hide") {
      show = false;
    }
  }

  /* IF FIELD HAS CONDITIONAL FIELD */
  if (field.condition) {
    if (field.condition.source == "form_data") {
      if (formData && formData[field.condition.name].toLowerCase() == field.condition.value) {
        show = true;
      } else {
        show = false;
      }
    } else {
      if (methods.getValues(field.condition.name) == field.condition.value) {
        show = true;
      } else {
        show = false;
      }
    }
  }

  /* IF FIELD HAS CUSTOM DEFAULT VALUE */
  if (field.custom_prefill) {
    /* DEFAULT VALUE USING SINGLE KEY ARRAY */
    if (field.custom_prefill.value_type == "index_key_array") {
      if (field.custom_prefill.source == "form_data" && formData && formData[field.custom_prefill.key]) {
        defaultValue =
          formData[field.custom_prefill.key][field.custom_prefill.index][field.custom_prefill.obj];
      } else {
        if (defaultValue && defaultValue[field.custom_prefill.index]) {
          defaultValue = defaultValue[field.custom_prefill.index][field.custom_prefill.key];
        }
      }
    }

    /* DEFAULT VALUE USING MULTIPLE KEYS OF ARRAY */
    if (field.custom_prefill.value_type == "index_keys_array") {
      if (
        field.custom_prefill.source == "form_data" &&
        formData &&
        formData[field.custom_prefill.source_key]
      ) {
        const value_keys = field.custom_prefill.value_keys;
        const source_key = field.custom_prefill.source_key;
        const source_index = field.custom_prefill.source_index;

        value_keys.forEach(function (key: any) {
          if (key.value == formData[source_key][source_index][key.key]) {
            defaultValue = formData[source_key][source_index][key.key];
          } else {
            if (formData[source_key][source_index][key.key]) {
              defaultValue = formData[source_key][source_index][key.key];
            }
          }
        });
      }
    }

    /* DEFAULT VALUE USING ARRAY */
    if (field.custom_prefill.value_type == "array") {
      let array: any[] = [];
      if (formData && field.custom_prefill.source == "form_data") {
        formData[field.custom_prefill.source_key].forEach(function (curr: any) {
          array.push(curr.currency);
        });
        defaultValue = array;
      }
    }

    /* DEFAULT VALUE USING ATTR KEY VALUE */
    if (field.custom_prefill.value_type == "attr_key_value") {
      if (formData && field.custom_prefill.source == "form_data") {
        let value_found = false;
        field.custom_prefill.value_options.forEach(function (option: any) {
          if (value_found) {
            return;
          }
          if (formData && field.custom_prefill.condition == "equal_to") {
            if (formData[field.custom_prefill.source_key] == option.value) {
              defaultValue = formData[option.key_value];
              value_found = true;
            } else {
              defaultValue = "";
            }
          }
          if (formData && field.custom_prefill.condition == "value_exist") {
            if (formData[field.custom_prefill.source_key]) {
              const key_value = field.custom_prefill.value_options.find(
                (element: any) => element.value == "exist"
              );
              defaultValue = formData[key_value.key_value];
            } else {
              const key_value = field.custom_prefill.value_options.find(
                (element: any) => element.value == "other"
              );
              defaultValue = formData[key_value.key_value];
            }
          }
        });
      }
    }
  }

  if (field.name == "aml_status") {
    defaultValue = defaultValue == "true" ? "Pass" : "Fail";
  }

  if (field.disabled_condition) {
    if (field.disabled_condition.type == "array") {
      let pass = true;
      const disabledList = field.disabled_condition.names;
      for (var i = 0; i < disabledList.length; i++) {
        if (!methods.getValues(disabledList[i])) {
          pass = false;
          break;
        } else {
          pass = true;
        }
      }
      disabled = !pass;
    } else {
      if (field.disabled_condition.value == "has_value" && methods.getValues(field.disabled_condition.name)) {
        disabled = field.disabled_condition.disable;
      }
    }
  }
  if (field.type == "text") {
    if (!show) return;
    let additionalValue = "";
    if (field.additional_type == "static") {
      additionalValue = field.additional_value;
    } else {
      additionalValue = methods.getValues(field.additional_value);
    }
    return (
      <Grid sm={6} item sx={{ display: !field.hidden ? "block" : "none" }}>
        <InputText
          parentCallback={() => parentCallback(field.callback.function)}
          additionalValue={field.additional_value ? additionalValue : null}
          additionalOptions={field.additional_options || []}
          validateInput={field.validate_input == true ? true : false}
          defaultValue={defaultValue}
          type={field.value_type}
          name={field.name}
          required={field.required}
          fullWidth={true}
          allowNegative={field.allow_negative || false}
          decimalScale={field.decimal_scale}
          label={translate(`dashboard.forms.labels.${field.name}`)}
          disabled={disabled}
          set={methods.setValue}
          get={methods.getValues}
          formData={formData}
        />
      </Grid>
    );
  }
  if (field.type == "select") {
    if (!show) return;
    return (
      <Grid sm={6} item sx={{ display: !field.hidden ? "block" : "none" }}>
        <InputSelect
          autocomplete={field.autocomplete || false}
          defaultValue={defaultValue}
          options={field.options ? field.options : []}
          name={field.name}
          required={field.required}
          fullWidth={true}
          label={translate(`dashboard.forms.labels.${field.name}`)}
          disabled={disabled}
          set={methods.setValue}
          hasSelectCallback={field.on_select_callback ? true : false}
          onSelectCallback={(value: any) => {
            if (field.on_select_callback) {
              onSelectCallback(field.on_select_callback, value);
            }
          }}
          selectedOptionValue={field.selected_option_value}
          get={methods.getValues}
          watch={methods.watch}
        />
      </Grid>
    );
  }
  if (field.type == "checkbox") {
    if (!show) return;
    return (
      <Grid sm={field?.options?.length > 3 ? 12 : 6} item sx={{ display: !field.hidden ? "block" : "none" }}>
        <InputCheckbox
          disabled={disabled}
          defaultValue={[defaultValue]}
          isMultiple={field.multiple_select}
          options={field.options}
          name={field.name}
          required={field.required}
          fullWidth={true}
          label={translate(`dashboard.forms.labels.${field.name}`)}
        />
      </Grid>
    );
  }
  if (field.type == "radio") {
    if (!show) return;
    return (
      <Grid sm={field?.options?.length > 3 ? 12 : 6} item sx={{ display: !field.hidden ? "block" : "none" }}>
        <InputRadio
          defaultValue={defaultValue}
          options={field.options}
          name={field.name}
          required={field.required}
          fullWidth={true}
          label={translate(`dashboard.forms.labels.${field.name}`)}
          addQuery={field.dependencies}
          set={methods.setValue}
        />
      </Grid>
    );
  }
  if (field.type == "image") {
    if (!show) return;
    return (
      <Grid sm={6} item sx={{ display: !field.hidden ? "block" : "none" }}>
        <InputFile
          defaultValue={defaultValue}
          callback={(name, value) => methods.setValue(name, value)}
          type={field.value_type}
          name={field.name}
          required={field.required}
          fullWidth={true}
          label={translate(`dashboard.forms.labels.${field.name}`)}
        />
      </Grid>
    );
  }

  if (field.type == "date") {
    if (!show) return;
    return (
      <Grid sm={6} item sx={{ display: !field.hidden ? "block" : "none" }}>
        <InputDate
          defaultValue={defaultValue}
          name={field.name}
          required={field.required}
          label={translate(`dashboard.forms.labels.${field.name}`)}
          get={methods.getValues}
          set={methods.setValue}
        />
      </Grid>
    );
  }

  if (field.type == "time") {
    if (!show) return;
    return (
      <Grid sm={6} item sx={{ display: !field.hidden ? "block" : "none" }}>
        <InputTime
          defaultValue={defaultValue}
          name={field.name}
          required={field.required}
          label={translate(`dashboard.forms.labels.${field.name}`)}
          get={methods.getValues}
          set={methods.setValue}
          watch={methods.watch}
        />
      </Grid>
    );
  }

  if (field.type == "date-time") {
    if (!show) return;

    return (
      <Grid sm={6} item sx={{ display: !field.hidden ? "block" : "none" }}>
        <InputDateTime
          name={field.name}
          label={translate(`dashboard.forms.labels.${field.name}`)}
          defaultValue={defaultValue}
          set={methods.setValue}
          watch={methods.watch}
          disabled={field.disabled}
          inputFormat={field.input_format}
          viewFormat={field.views_format}
        />
      </Grid>
    );
  }

  if (field.type === "editor") {
    if (!show) return;
    return (
      <Grid sm={12} item sx={{ display: !field.hidden ? "block" : "none" }}>
        <FormLabel color="secondary">{translate(`dashboard.forms.labels.${field.name}`)}</FormLabel>
        <div style={{ background: palette.brandGrey[500], minHeight: "300px" }}>
          <Editor
            editorState={editorState}
            toolbarClassName="toolbarClassName"
            wrapperClassName="wrapperClassName"
            editorClassName="editorClassName"
            onEditorStateChange={onEditorStateChange}
          />
        </div>
      </Grid>
    );
  }

};

const DetailContentForm: FC<{
  fields: any;
  validations: any;
  initialData?: any;
  submitButtonCaption?: string;
  onSubmit?: (value: any, methods: any) => void;
  onCancel?: () => void;
  formData?: any;
  requestError?: any;
  fetchingRequest?: boolean;
  isDisabledButton?: boolean;
  parentCallback?: (func: any, data: any, methods: any) => void;
  onSelectCallback?: (func: any, data: any, value: any, methods: any) => void;
  detailAction?: (value: any) => void;
}> = ({
  fields,
  validations,
  initialData,
  onSubmit,
  submitButtonCaption,
  formData = null,
  isDisabledButton = false,
  requestError = null,
  onCancel = () => null,
  fetchingRequest = false,
  parentCallback = () => null,
  onSelectCallback = () => null,
  detailAction = () => null
}) => {
  const {
    query: { id, feature, page },
    back,
    push
  } = useRouter();
  const { translate } = useTranslate();
  const methods = useForm<any>({
    resolver: zodResolver(validations)
  });
  const { client_functions } = securityStore((state) => state, shallow) as SecurityStoreTypes;
  const access = client_functions;

  const checkAccess = (row: any, permission: "writeAccess" | "readAccess" | "approveAccess") => {
    if (access) {
      if (row?.entityId) {
        const rowAccess = access[row.entityId]?.find(
          (menu: any) => convertNameToUrl(menu.funcNameEn) == feature
        );
        return rowAccess ? rowAccess[permission] : false;
      } else {
        return true;
      }
    }
    return true;
  };

  const {
    reset,
    handleSubmit,
    getValues,
    setValue,
    watch,
    formState: { errors },
    setError
  } = methods;
  type FormInput = TypeOf<typeof validations>;
  const isWithdrawalDetail = feature === "withdrawal-management" && page === "detail";
  const isWithdrawalApproval = isWithdrawalDetail && watch("status") == 0;
  const isDepositDetail = feature === "deposit-management" && page === "detail";
  const isDepositApproval = isDepositDetail && watch("status") == 0;

  const [amlStatus, setAmlStatus] = useState<any>("");
  const [rejectModal, setRejectModal] = useState<boolean>(false);

  const onSubmitHandler: SubmitHandler<FormInput> = (data) => {
    const systemSettingMenu = [
      "user-management",
      "role-settings",
      "list-of-key-term-naming",
      "prc-province",
      "prc-region-district",
      "prc-city",
      "manual-adjustments-types",
      "local-bank-list-edda-mapping",
      "dummy-account-list",
      "bank-&-cc-registration-limit",
      "operator-notifications",
      "trade-date"
    ];

    const isHaveAccess = client_functions[data.entity]?.find(
      (menu: any) => convertNameToUrl(menu.funcNameEn) == feature
    ).writeAccess;

    if (onSubmit) {
      if (!systemSettingMenu.includes(feature as string)) {
        if (isHaveAccess) {
          onSubmit(data, methods);
        } else {
          toast.error(`Dont have write access for this entity`, {
            duration: 5000,
            position: "top-right",
            style: {
              background: "#ffe3e3",
              color: "#000000"
            }
          });
        }
      } else {
        const entities = Object.keys(client_functions);
        let writeAccess = 0;

        entities.map((el: any) => {
          const access = client_functions[el].find(
            (x: any) => convertNameToUrl(x.funcNameEn) == feature
          )?.writeAccess;
          if (access) {
            writeAccess++;
          }
        });

        if (writeAccess > 0) {
          onSubmit(data, methods);
        } else {
          toast.error(`Dont have write access for this function`, {
            duration: 5000,
            position: "top-right",
            style: {
              background: "#ffe3e3",
              color: "#000000"
            }
          });
        }
      }
    }
  };

  const { getItem } = useLocalStorage();
  const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);
  const isOwnData = account == watch("request_by");

  return (
    <Stack>
      <Card sx={{ mb: 10 }}>
        <FormProvider {...methods}>
          <Stack sx={{ p: 2 }}>
            <Typography variant="h4">{translate(`dashboard.labels.${page}`)}</Typography>
          </Stack>
          <Divider />
          <Box component="form" noValidate autoComplete="off" onSubmit={handleSubmit(onSubmitHandler)}>
            <CardContent>
              <Grid container spacing={2}>
                {fields &&
                  fields?.map((field: any, key: any) => {
                    return (
                      /* @ts-ignore */
                      <RenderForm
                        key={key}
                        field={field}
                        defaultValue={initialData ? initialData[field.name] : ""}
                        methods={methods}
                        formData={formData}
                        setError={setError}
                        parentCallback={(func: any) => parentCallback(func, getValues(), methods)}
                        onSelectCallback={(func: any, value: any) => {
                          onSelectCallback(func, getValues(), value, methods);
                        }}
                        getAmlStatus={(val) => {
                          setAmlStatus(val);
                        }}
                      />
                    );
                  })}
              </Grid>
              {feature === "operator-notifications" && (
                <Button onClick={detailAction} sx={{ mt: requestError ? 2 : 3, mb: 2 }}>
                  <Typography variant="body" fontWeight={700}>
                    hints
                  </Typography>
                </Button>
              )}
            </CardContent>
            <Divider />
            {requestError && (
              <Stack>
                <Typography
                  variant="body"
                  color="brandRed.500"
                  textAlign={"right"}
                  sx={{ mt: 2, mr: 2 }}
                >{`${requestError.errorDescription}`}</Typography>
              </Stack>
            )}
            <Stack direction={"row"} gap={2} justifyContent={"flex-end"} sx={{ pr: 2 }}>
              <Button
                onClick={function () {
                  onCancel();
                  push(`/${feature}/management`);
                }}
                sx={{ mt: requestError ? 2 : 3, mb: 2 }}
                variant={"outlined"}
              >
                <Typography variant="body" fontWeight={700}>
                  {translate("dashboard.buttons.cancel")}
                </Typography>
              </Button>
              {onSubmit && (
                  <Button
                    type="submit"
                    sx={{ mt: requestError ? 2 : 3, mb: 2 }}
                    disabled={fetchingRequest || isDisabledButton}
                  >
                    <Typography variant="body" fontWeight={700}>
                      {translate(`dashboard.buttons.${submitButtonCaption ? submitButtonCaption : "submit"}`)}
                    </Typography>
                  </Button>
                )}

              {checkAccess(formData, "approveAccess") && isWithdrawalApproval && !isOwnData && (
                <>
                  <Button
                    onClick={() => detailAction({ action: "approve" })}
                    sx={{ mt: requestError ? 2 : 3, mb: 2 }}
                    disabled={fetchingRequest || amlStatus.toLowerCase() === "fail"}
                  >
                    <Typography variant="body" fontWeight={700}>
                      {translate(`dashboard.buttons.approve`)}
                    </Typography>
                  </Button>
                  <Button
                    onClick={() => setRejectModal(true)}
                    sx={{ mt: requestError ? 2 : 3, mb: 2 }}
                    disabled={fetchingRequest}
                  >
                    <Typography variant="body" fontWeight={700}>
                      {translate(`dashboard.buttons.reject`)}
                    </Typography>
                  </Button>
                </>
              )}

              {checkAccess(formData, "approveAccess") && isDepositApproval && !isOwnData && (
                <>
                  <Button
                    onClick={() => detailAction({ action: "approve" })}
                    sx={{ mt: requestError ? 2 : 3, mb: 2 }}
                    disabled={fetchingRequest || amlStatus.toLowerCase() === "fail"}
                  >
                    <Typography variant="body" fontWeight={700}>
                      {translate(`dashboard.buttons.approve`)}
                    </Typography>
                  </Button>
                  <Button
                    onClick={() => setRejectModal(true)}
                    sx={{ mt: requestError ? 2 : 3, mb: 2 }}
                    disabled={fetchingRequest}
                  >
                    <Typography variant="body" fontWeight={700}>
                      {translate(`dashboard.buttons.reject`)}
                    </Typography>
                  </Button>
                </>
              )}
              <RejectModal
                data={initialData}
                isOpen={rejectModal}
                onClose={() => setRejectModal(false)}
                onSubmit={detailAction}
              />
            </Stack>
          </Box>
        </FormProvider>
      </Card>
    </Stack>
  );
};

export default DetailContentForm;
