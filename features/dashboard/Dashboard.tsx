import { FC } from "react";

import { Typography, Stack, Container } from "@mui/material";
import DashboardTitle from "./components/DashboardTitle";

const Dashboard: FC = () => {
  return (
    <Container maxWidth={false}>
      <DashboardTitle />
    </Container>
  );
};

export default Dashboard;
