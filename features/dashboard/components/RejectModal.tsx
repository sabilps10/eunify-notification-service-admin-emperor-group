import * as React from "react";
import { Stack, TextField } from "@mui/material";
import useTranslate from "@/hooks/useTranslate";
import ModalDialog from "../../../components/ModalDialog";

interface RejectModalProps {
  data: any;
  isOpen: boolean;
  onClose: () => void;
  onSubmit: (payload: any) => void;
}

const RejectModal: React.FC<RejectModalProps> = ({ data, isOpen, onClose, onSubmit }) => {
  const { translate } = useTranslate();
  const [message, setMessage] = React.useState<string>("");

  const handleSubmit = () => {
    onSubmit({ data: { ...data, comment: message }, action: "reject" });
    onClose();
  };

  return (
    <ModalDialog
      isOpen={isOpen}
      onClose={onClose}
      onCallback={handleSubmit}
      title="Reject message"
      buttons={{
        continue: translate("dashboard.buttons.confirm"),
        cancel: translate("dashboard.buttons.cancel")
      }}
      disabledContinue={!message}
    >
      <Stack sx={{ width: 600 }}>
        <TextField
          required={true}
          multiline
          fullWidth={true}
          inputProps={{ maxLength: 255 }}
          onChange={(e) => setMessage(e.target.value)}
        />
      </Stack>
    </ModalDialog>
  );
};

export default RejectModal;
