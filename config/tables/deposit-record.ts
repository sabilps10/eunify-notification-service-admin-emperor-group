export const DEPOSIT_HEADER = [
  {
    id: "tradeTypeName",
    numeric: false,
    disablePadding: false,
    label: "Type",
    isSort: true,
    sortId: "tradeType"
  },
  {
    id: "adjustmentTypeName",
    numeric: false,
    disablePadding: false,
    label: "Adjustment Type",
    isSort: true,
    sortId: "adjustmentType"
  },
  {
    id: "orderNo",
    numeric: false,
    disablePadding: false,
    label: "Reference No",
    isSort: true
  },
  {
    id: "entityId",
    numeric: false,
    disablePadding: false,
    label: "Entity",
    isSort: true
  },
  {
    id: "tradingAccountNo",
    numeric: false,
    disablePadding: false,
    label: "Trading Acc",
    isSort: true
  },
  {
    id: "clientName",
    numeric: false,
    disablePadding: false,
    label: "Acc Holder",
    isSort: true,
    sortId: "customerBankAccount.accHolderName"
  },
  {
    id: "isValidAml",
    numeric: false,
    disablePadding: false,
    label: "AML Status",
    isSort: true,
    sortId: "validAml"
  },
  {
    id: "balance",
    name: "balance",
    numeric: false,
    disablePadding: false,
    label: "Balance",
    isSort: true
  },
  {
    id: "custom_value",
    name: "platform_amount",
    numeric: false,
    disablePadding: false,
    label: "Platform Amount",
    isSort: true,
    sortId: "depositAmount"
  },
  {
    id: "custom_value",
    name: "actual_currency",
    numeric: false,
    disablePadding: false,
    label: "Actual Currency",
    isSort: false
  },
  {
    id: "custom_value",
    name: "actual_amount",
    numeric: false,
    disablePadding: false,
    label: "Actual Amount",
    isSort: true,
    sortId: "amount"
  },
  {
    id: "paymentChannelName",
    numeric: false,
    disablePadding: false,
    label: "Deposit Channel",
    isSort: false
  },
  {
    id: "createDate",
    numeric: false,
    disablePadding: false,
    label: "Application Time",
    isSort: true
  },
  {
    id: "approvedDate",
    numeric: false,
    disablePadding: false,
    label: "Approval Time",
    isSort: true
  },
  {
    id: "comment",
    numeric: false,
    disablePadding: false,
    label: "Remarks",
    isSort: false
  },
  {
    id: "requestBy",
    numeric: false,
    disablePadding: false,
    label: "Requested By",
    isSort: true
  },
  {
    id: "statusName",
    numeric: false,
    disablePadding: false,
    label: "Status",
    isSort: true,
    sortId: "status"
  },
  {
    id: "action",
    numeric: false,
    disablePadding: false,
    label: "Action",
    key_value: "orderNo",
    sticky: true,
    isSort: false,
    idValue: "id"
  }
];

export const DEPOSIT_ROWS = [
  "type",
  "adjustment_type",
  "order_no",
  "entity",
  "trading_acc",
  "acc_holder",
  "aml_status",
  "balance",
  "ref_deposit_amount",
  "settlement_currency",
  "settlement_amount",
  "deposit_channel",
  "application_time",
  "approval_time",
  "requested_by",
  "remarks",
  "status"
];

export const DEPOSIT_ACTION = [
  { name: "add", action: { type: "link", value: "/add" } },
  { name: "export", action: { type: "export_csv", value: "" } }
];

export const DEPOSIT_EXPORT_HEADING = [
  { name: "tradeTypeName", label: "Type", width: 20, position: 1 },
  { name: "adjustmentTypeName", label: "Adjustment Type", width: 20, position: 2 },
  { name: "orderNo", label: "Reference No", width: 20, position: 3 },
  { name: "entityId", label: "Entity", width: 20, position: 4 },
  { name: "tradingAccountNo", label: "Trading Acc No", width: 20, position: 5 },
  { name: "clientName", label: "Acc Holder Name", width: 20, position: 6 },
  {
    name: "isValidAml",
    label: "AML Status",
    type: "custom-value",
    custom_values: [
      { value: true, label: "Pass" },
      { value: false, label: "Fail" }
    ],
    width: 20,
    position: 7
  },
  { name: "balance", label: "Balance", width: 20, position: 8 },
  { type: "custom-data", key: "platform-amount", label: "Platform Amount", width: 20, position: 9 },
  { type: "custom-data", key: "exchange-rate", label: "Ref Exchange rate", width: 20, position: 10 },
  { name: "currency", label: "Actual Currency", width: 20, position: 11 },
  { name: "tmpAmount", label: "Actual Amount", width: 20, position: 12 },
  { name: "tmpCharge", label: "Ref Service Charge", width: 20, position: 13 },
  // {
  //   type: "custom-data",
  //   key: "calculated-actual-deposit",
  //   label: "Calculated Actual Deposit Amount",
  //   width: 20,
  //   position: 14
  // },
  { name: "paymentChannelName", label: "Deposit Channel", width: 20, position: 14 },
  { name: "bankName", label: "Bank Name", width: 20, position: 15 },
  { name: "bankAccountNo", label: "Bank Account No", width: 20, position: 16 },
  { name: "accHolderName", label: "Bank Account Holder Name", width: 20, position: 17 },
  { name: "bankAccountNo", label: "Credit Card Number", width: 20, position: 18 },
  { name: "cryptoAddress", label: "Crypto Address", width: 20, position: 19 },
  // { name: "tradeDate", label: "Trade Date", width: 20, position: 21 },
  { name: "createDate", label: "Application time", width: 20, position: 20 },
  { name: "approvedDate", label: "Approval Time", width: 20, position: 21 },
  { name: "requestBy", label: "Request by", width: 20, position: 22 },
  { name: "approvedBy", label: "Approved by", width: 20, position: 23 },
  { name: "comment", label: "Remarks", width: 20, position: 24 },
  { name: "statusName", label: "status", width: 20, position: 25 }
];
