import { FC, useState, useEffect } from "react";
import { useForm, SubmitHandler, FormProvider } from "react-hook-form";
import { useRouter } from "next/router";
import { TypeOf } from "zod";

import Alert from "@mui/material/Alert";
import Button from "@mui/material/Button";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { Stack, CircularProgress } from "@mui/material";

import InputText from "@/components/FormFields/InputText";
import { zodResolver } from "@hookform/resolvers/zod";

import useAmazonCognito from "@hooks/useAmazonCognito";
import { loginSchema } from "@/config/form-fields/auth";
import useLocalStorage from "@/hooks/useLocalStorage";
import { LOCALSTORAGE_KEY } from "@/config/constants";

import Image from 'next/image'
import logo from "../../public/efsg.png";

type LoginInput = TypeOf<typeof loginSchema>;
const Login: FC = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<string | null>(null);
  const { push } = useRouter();
  const { login } = useAmazonCognito();
  const methods = useForm<LoginInput>({
    resolver: zodResolver(loginSchema),
    defaultValues: {
      username: "",
      password: ""
    }
  });
  const { reset, handleSubmit, setValue } = methods;
  const { removeItem } = useLocalStorage();
  
  useEffect(() => {
    removeItem(LOCALSTORAGE_KEY.TOKEN);
    removeItem(LOCALSTORAGE_KEY.REFRESH_TOKEN);
    removeItem(LOCALSTORAGE_KEY.ACCOUNT);
  }, []);

  const onSubmitHandler: SubmitHandler<LoginInput> = (values) => {
    setLoading(true);
    login(values.username, values.password)
      .catch((err) => {
        setErrorMessage(err.message);
        reset({
          username: "",
          password: ""
        });
      })
      .finally(() => {
        setLoading(false);
      });
  };
  return (
    <Container component="main" maxWidth="xs">
      <FormProvider {...methods}>
        <Box
          component="form"
          noValidate
          autoComplete="off"
          onSubmit={handleSubmit(onSubmitHandler)}
          sx={{
            background: "#FFF",
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "stretch",
            borderRadius: 3,
            padding: 3,
            boxShadow: 1
          }}
        >
          <Stack gap={2} alignItems={"center"}>
            <Stack style={{width: 250}}>
            <Image
              src={logo}
              height={200}
            />
            </Stack>
            <Stack width={"100%"}>
              <Typography variant="h3" textAlign="center" marginBottom={3}>
                Sign in to Dashboard
              </Typography>
              {errorMessage && <Alert severity="error">{errorMessage}</Alert>}
              <Stack gap={2}>
                <InputText
                  disabled={loading}
                  name="username"
                  required={true}
                  fullWidth={true}
                  label={"Username"}
                  set={setValue}
                />
                <InputText
                  disabled={loading}
                  name="password"
                  required={true}
                  fullWidth={true}
                  label={"Password"}
                  additionalRegex={false}
                  set={setValue}
                />
              </Stack>
            </Stack>
          </Stack>

          <Button type="submit" fullWidth sx={{ mt: 3, mb: 2 }} disabled={loading}>
            {loading ? (
              <CircularProgress size={22} />
            ) : (
              <Typography variant="body" fontWeight={700}>
                Sign in
              </Typography>
            )}
          </Button>
        </Box>
      </FormProvider>
    </Container>
  );
};

export default Login;
