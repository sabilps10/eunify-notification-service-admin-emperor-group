import { FC, PropsWithChildren, useMemo } from "react";

import {
  TextField,
  FormLabel,
  TextFieldProps,
  Typography,
  FormControl,
  Stack,
  MenuItem
} from "@mui/material";
import { Controller, useFormContext } from "react-hook-form";
import CustomNumberFormat from "./NumberFormat";

export type InputTextProps = {
  label?: string;
  fullWidth?: boolean;
  required?: boolean;
  name: string;
  sx?: any;
  type?: string;
  callback?: (name: any, value: any) => void;
} & TextFieldProps;

const InputFile: FC<InputTextProps> = ({
  label,
  fullWidth,
  required,
  name,
  sx,
  type,
  callback,
  ...props
}) => {
  const {
    control,
    formState: { errors }
  } = useFormContext();

  const error = errors[name] ? errors[name]?.message : "";
  return (
    <Controller
      control={control}
      name={name}
      defaultValue=""
      render={({ field }) => {
        return (
          <>
            <FormControl sx={{ gap: 1 }} fullWidth error={!!errors[name]}>
              {label && <FormLabel color="secondary">{label}</FormLabel>}
              <Stack direction={"row"} gap={1}>
                {type == "number-currency" && <TextField value={"USDT"} sx={{ width: 100 }} />}
                <TextField
                  {...props}
                  {...field}
                  value=""
                  type="file"
                  onChange={(e: any) => {
                    if (callback) callback(name, e.target.files[0].name);
                    field.onChange(e.target.files[0]);
                  }}
                  sx={{
                    border: "solid",
                    borderWidth: !!errors[name] ? 1 : 0,
                    borderColor: "red",
                    ...sx
                  }}
                  fullWidth={fullWidth}
                  required={required}
                />
              </Stack>
              <Typography variant="body" color="brandRed.500">{`${error}`}</Typography>
            </FormControl>
          </>
        );
      }}
    ></Controller>
  );
};

export default InputFile;
