export const RECONCILIATION_REPORT_FILTER = [
  { id: "report_name", type: "dropdown" },
  { id: "period", type: "time-range" }
];

export const RECONCILIATION_REPORT_ACTION = [{ name: "export" }];
