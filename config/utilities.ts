export const snakeToCamel = (str: string) =>
  str.toLowerCase().replace(/([-_][a-z])/g, (group) => group.toUpperCase().replace("-", "").replace("_", ""));

export const snakeTotitleCaseText = (s: string) =>
  s.replace(/^_*(.)|_+(.)/g, (s, c, d) => (c ? c.toUpperCase() : " " + d.toUpperCase()));

export const formatNumber = (currency: string, value: any) => {
  if (!currency && !value) return 0;
  return new Intl.NumberFormat().format(value).trim();
};

export const GenerateSelectOptions = (items: any, value: string, label: string[]) => {
  const options: { value: any; label: string }[] = [];
  items?.forEach(function (item: any) {
    const option = { value: item[value], label: "" };
    label.forEach(function (label: any) {
      option.label += " " + item[label];
    });
    options.push(option);
  });
  return options;
};

export const ShowDataValueWithEmpty = (value: any) => {
  if (value) return value;
  return "N/A";
};

export const PaymentUnit = (type: string, currency: any) => {
  switch (type) {
    case "P":
      return "%";
      break;
    case "D":
      return currency;
    default:
      return "";
      break;
  }
};

export const FindIndexFormField = (fields: any, key: string, attribute?: "name" | "id") => {
  const attr = attribute ? attribute : "name";
  const index = fields.findIndex((field: any) => field[attr] == key);
  return index;
};

export const HumanizedDifference2Objects = (action: any, beforeObject: any, afterObject: any) => {
  const before = beforeObject;
  const after = afterObject;
  let text = "";
  if (action.toLowerCase() == "insert") {
    for (const property in after) {
      text += "<tr style='margin-bottom: 5px'>";
      text += "<td style='padding-bottom:5px; padding-top: 5px;'>";
      text += `<span style="background: #e3fcef">New [${property}] ${after[property]}</span>`;
      text += "</td>";
      text += "</tr>";
    }
  }
  if (action.toLowerCase() == "update") {
    for (const property in before) {
      if (after?.hasOwnProperty(property)) {
        if (before[property] !== after[property]) {
          text += "<tr style='margin-bottom: 5px'>";
          text += "<td style='padding-bottom:10px; padding-top: 10px;'>";
          text += `<span style="background: #ffebe6">Original [${property}] ${before[property]}; </span>`;
          text += "</td>";
          text += `<td style="width: 50px; text-align: center; padding-bottom:10px; ">`;
          text += "&#8594";
          text += "<td>";
          text += "<td style='padding-bottom:10px; padding-top: 10px;'>";
          text += `<span style="background: #e3fcef;">New [${property}] ${after[property]}</span>`;
          text += "</td>";
          text += "</tr>";
        }
      }
    }
    text += "</table>";
  }
  return text;
};

export function RoundNumber(num: number) {
  return (Math.round(Number(num) * 100) / 100).toFixed(2);
}
