import { useState, useEffect } from "react";
// @ts-ignore
import * as XLSX from "xlsx";

const useExportFile = () => {
  const onGenerateFile = (data: any, heading: any, filename: string, ext: string) => {
    const fileType =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8";
    const fileExtension = ext;
    const wscolsWidth: any[] = [];
    const wscols: any = [];

    heading.forEach(function (head: any) {
      wscols.push(head.col);
      wscolsWidth.push({ width: head.width });
    });


    const wb = XLSX.utils.book_new();
    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet([]);

    XLSX.utils.sheet_add_aoa(ws, [wscols]);
    XLSX.utils.sheet_add_json(ws, data, { origin: "A2", skipHeader: true });
    XLSX.utils.book_append_sheet(wb, ws, "Sheet1");
    //@ts-ignore
    ws["!cols"] = wscolsWidth;

    XLSX.writeFile(wb, filename + fileExtension);
  };
  return {
    onGenerateFile
  };
};

export default useExportFile;
