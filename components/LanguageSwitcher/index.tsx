import { FC } from "react";

type LanguageSwitcherProps = {
  onSwitch: (lang: string) => void;
};

const LanguageSwitcher: FC<LanguageSwitcherProps> = ({
  onSwitch = () => null
}) => {
  // TODO: add two button that listen this event, e.g
  // () => onSwitch("en-US")
  return null;
};

export default LanguageSwitcher;
