export const TRANSACTIONS_ACTION = [{ name: "export", action: { type: "export_csv", value: "" } }];

export const TRANSACTIONS_HEADER = [
  {
    id: "entityId",
    numeric: false,
    disablePadding: false,
    label: "Entity",
    isSort: true
  },
  {
    id: "paymentChannelName",
    numeric: false,
    disablePadding: false,
    label: "Channel",
    isSort: true
  },
  {
    id: "orderNo",
    numeric: true,
    disablePadding: false,

    label: "Reference No",
    isSort: true
  },
  {
    id: "tradingAccountNo",
    numeric: true,
    disablePadding: false,
    label: "Trading Account No.",
    isSort: true
  },
  {
    id: "IBAccountNo",
    numeric: true,
    disablePadding: false,
    label: "IB Code",
    isSort: true
  },
  {
    id: "tradeTypeName",
    numeric: false,
    disablePadding: false,
    label: "Type",
    isSort: true,
    sortId: "tradeType"
  },
  {
    id: "custom_value",
    name: "platform_amount",
    numeric: true,
    disablePadding: false,
    label: "Platform Amount",
    isSort: true
  },
  {
    id: "custom_value",
    name: "actual_currency",
    numeric: false,
    disablePadding: false,
    label: "Actual Currency",
    isSort: true
  },
  {
    id: "custom_value",
    name: "actual_amount",
    numeric: true,
    disablePadding: false,
    label: "Actual Amount",
    isSort: true
  },
  {
    id: "settlementAccCurrency",
    name: "account_settlement_currency",
    numeric: true,
    disablePadding: false,
    label: "Account Settlement Currency",
    isSort: true
  },
  {
    id: "custom_value",
    name: "service_fee",
    numeric: true,
    disablePadding: false,
    label: "Service Fee",
    isSort: true
  },
  {
    id: "createdDate",
    numeric: false,
    disablePadding: false,
    label: "Application Time",
    isSort: true
  },
  {
    id: "txnDate",
    numeric: false,
    disablePadding: false,
    label: "Transaction Time",
    isSort: true
  },
  {
    id: "tradeDate",
    numeric: false,
    disablePadding: false,
    label: "Trade Date",
    isSort: true
  },
  {
    id: "comment",
    numeric: false,
    disablePadding: false,
    label: "Remark",
    isSort: true
  },
  {
    id: "paymentStatusName",
    numeric: false,
    disablePadding: false,
    label: "Status",
    isSort: true,
    sortId: "paymentStatus"
  },
  {
    id: "action",
    numeric: false,
    disablePadding: false,
    label: "Action",
    key_value: "orderNo",
    isSort: true,
    idValue: "id"
  }
];

export const TRANSACTION_EXPORT_HEADING = [
  { name: "tradeTypeName", label: "Type", width: 20, position: 1, isSort: true },
  { name: "adjustmentTypeName", label: "Adjustment Type", width: 20, position: 2, isSort: true },
  { name: "orderNo", label: "Order No.", width: 35, position: 3, isSort: true },
  { name: "entityId", label: "Entity", width: 20, position: 4, isSort: true },
  { name: "paymentChannelName", label: "Channel", width: 20, position: 5, isSort: true },
  { name: "tradingAccountNo", label: "Trading Acc No.", width: 30, position: 6, isSort: true },
  { name: "clientName", label: "Acc Holder Name", width: 30, position: 7, isSort: true },
  {
    name: "validAml",
    label: "AML Status",
    type: "custom-value",
    custom_values: [
      { value: true, label: "Pass" },
      { value: false, label: "Fail" }
    ],
    width: 20,
    position: 8,
    isSort: true
  },
  { name: "settlementAccCurrency", label: "Account Settlement Currency", width: 20, position: 9 },
  {
    type: "custom-data",
    key: "exchange-rate",
    label: "Exchange Rate",
    width: 20,
    position: 10,
    isSort: true
  },
  { type: "custom-data", key: "platform-currency", label: "Platform Currency", width: 20, position: 11 },
  { type: "custom-data", key: "platform-amount", label: "Platform Amount", width: 20, position: 12 },
  { type: "custom-data", key: "actual-currency", label: "Actual Currency", width: 20, position: 13 },
  { type: "custom-data", key: "actual-amount", label: "Actual Amount", width: 20, position: 14 },
  {
    type: "custom-data",
    key: "service-charge",
    label: "Service Charge",
    width: 20,
    position: 15,
    isSort: true
  },
  {
    type: "custom-data",
    key: "calculated-actual-deposit",
    label: "Calculated Actual Deposit Amount",
    width: 20,
    position: 16
  },
  { name: "refId", label: "Payment gateway ref no", width: 20, position: 17, isSort: true },
  { name: "bankName", label: "Bank name", width: 20, position: 18, isSort: true },
  {
    type: "custom-data",
    key: "bank-location",
    name: "bankLocation",
    label: "Bank Location",
    width: 20,
    position: 19
  },
  {
    type: "custom-data",
    key: "bank-province",
    name: "bankProvince",
    label: "Bank Province",
    width: 20,
    position: 20
  },
  {
    type: "custom-data",
    key: "bank-region",
    name: "bankRegion",
    label: "Bank Region",
    width: 20,
    position: 21
  },
  { type: "custom-data", key: "bank-city", name: "bankCity", label: "Bank City", width: 20, position: 22 },
  { name: "bankAddress", label: "Bank Address", width: 20, position: 23 },
  { name: "accHolderName", label: "Bank Account Holder Name", width: 20, position: 24, isSort: true },
  { name: "bankAccountNo", label: "Credit card no", width: 20, position: 25, isSort: true },
  { name: "IBAccountNo", label: "IB Code", width: 20, position: 26 },
  { name: "cryptoNetwork", label: "network", width: 20, position: 27, isSort: true },
  { name: "cryptoAddress", label: "Crypto address", width: 20, position: 28, isSort: true },
  { name: "tradeDate", label: "Trade date", width: 20, position: 29, isSort: true },
  { name: "createdDate", label: "Application time", width: 20, position: 30, isSort: true },
  { name: "approvedDate", label: "Approval time", width: 20, position: 31, isSort: true },
  { name: "txnDate", label: "Transaction time", width: 20, position: 32, isSort: true },
  { name: "settleDate", label: "Payment result time", width: 20, position: 33, isSort: true },
  { name: "createBy", label: "Request by", width: 30, position: 34, isSort: true },
  { name: "approvedBy", label: "Approved by", width: 20, position: 35, isSort: true },
  { name: "lastModifiedBy", label: "Payment completed by", width: 20, position: 36, isSort: true },
  { name: "paymentStatusName", label: "status", width: 30, position: 37, isSort: true },
  { name: "comment", label: "remarks", width: 50, position: 38, isSort: true }
];
