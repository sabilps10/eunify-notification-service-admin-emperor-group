import type { FC, PropsWithChildren } from "react";
import { useRouter } from "next/router";

import {
  FormControl,
  FormLabel,
  Typography,
  TextField,
  MenuItem,
  TextFieldProps,
  RadioGroup,
  FormControlLabel,
  Radio,
  Stack
} from "@mui/material";
import { Controller, useFormContext } from "react-hook-form";

export type InputRadioProps = {
  label?: string;
  fullWidth?: boolean;
  required?: boolean;
  name: string;
  options: any;
  sx?: any;
  size?: string;
  direction?: "horizontal" | "vertical";
  defaultValue?: any;
  addQuery?: boolean;
  set?: any;
} & TextFieldProps;

const InputRadio: FC<InputRadioProps> = ({
  label,
  fullWidth,
  required,
  name,
  sx,
  options,
  size,
  direction = "horizontal",
  defaultValue,
  addQuery,
  set,
  ...props
}) => {
  const {
    control,
    formState: { errors }
  } = useFormContext();
  const { push, query } = useRouter();

  const error = errors[name] ? errors[name]?.message : "";

  const handleChange = (val: string) => {
    set(name, val, { shouldValidate: true });
    if (addQuery) {
      push({ pathname: `/${query.feature}/${query.page}`, query: { ...query, [name]: val } });
    }
    return;
  };

  return (
    <Controller
      control={control}
      name={name}
      render={({ field }) => {
        let stackDirection = "row";
        if (direction == "horizontal") {
          stackDirection = "row";
        }
        if (direction == "vertical") {
          stackDirection = "column";
        }
        return (
          <>
            <FormControl sx={{ gap: 1 }} fullWidth required={required} error={!!errors[name]}>
              {label && <FormLabel color="secondary">{label}</FormLabel>}
              <RadioGroup
                {...field}
                value={field.value || ""}
                defaultValue={defaultValue || ""}
                onChange={(e) => handleChange(e.target.value)}
              >
                {/* @ts-ignore */}
                <Stack direction={stackDirection}>
                  {options.map((value: any, key: any) => {
                    return (
                      <FormControlLabel
                        checked={value.value == field.value ? true : false}
                        key={key}
                        value={value.value || ""}
                        control={<Radio />}
                        label={value.label}
                      />
                    );
                  })}
                </Stack>
              </RadioGroup>
              <Typography variant="body" color="brandRed.500">{`${error}`}</Typography>
            </FormControl>
          </>
        );
      }}
    ></Controller>
  );
};

export default InputRadio;
