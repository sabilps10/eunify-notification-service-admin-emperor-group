import {
  CognitoUser,
  AuthenticationDetails,
  CognitoRefreshToken,
  CookieStorage
} from "amazon-cognito-identity-js";
import { useRouter } from "next/router";
import { format } from "date-fns";
import userPool from "@/userPool";
import useLocalStorage from "./useLocalStorage";
import { LOCALSTORAGE_KEY } from "@/config/constants";
import { reject } from "lodash";
import shallow from "zustand/shallow";

const useAmazonCognito = () => {
  const { setItem, removeItem, getItem } = useLocalStorage();
  const {push} = useRouter()
  const register = (email: string, password: string) => {
    userPool.signUp(email, password, [], [], (err, data) => {
      if (err) {
        alert("unknown error");
      }
    });
  };


  const login = (email: string, password: string) => {
    return new Promise((res, rej) => {
      const user = new CognitoUser({
        Username: email,
        Pool: userPool,
        Storage: new CookieStorage({
          domain: process.env.NEXT_PUBLIC_DOMAIN ? process.env.NEXT_PUBLIC_DOMAIN : "http://localhost:3000",
          secure: true
        })
      });
      const authDetails = new AuthenticationDetails({
        Username: email,
        Password: password
      });
      user.authenticateUser(authDetails, {
        onSuccess: async (data: any) => {
          const unix = data.accessToken.payload.exp;

          setItem(LOCALSTORAGE_KEY.TOKEN, data.accessToken.jwtToken);
          setItem(LOCALSTORAGE_KEY.REFRESH_TOKEN, data.refreshToken.token);
          setItem(LOCALSTORAGE_KEY.ACCOUNT, data.accessToken.payload.username);
          setItem(LOCALSTORAGE_KEY.EXPIRED_TIME, unix);

          push("/report-management/management");

        },
        onFailure: (err) => {
          rej(err);
        }
      });
    });
  };

  const getSession = async () => {
    return await new Promise((res, reject) => {
      const user = userPool.getCurrentUser();
      if (user) {
        user.getSession((err: any, session: any) => {
          if (err) {
            reject();
          } else {
            res(session);
          }
        });
      } else {
        reject();
      }
    });
  };

  const refreshToken = async (token: any, email: string) => {
    const refresh = new CognitoRefreshToken({ RefreshToken: token });
    const user = new CognitoUser({
      Username: email,
      Pool: userPool
    });
    user.refreshSession(refresh, (err, session) => {
      if (err) {
        logout();
      } else {
        console.log(session);
      }
    });
  };

  const logout = () => {
    const user = userPool.getCurrentUser();
    const account = getItem(LOCALSTORAGE_KEY.ACCOUNT);

    if (user) {
      user.signOut();
    }
    removeItem(LOCALSTORAGE_KEY.TOKEN);
    removeItem(LOCALSTORAGE_KEY.REFRESH_TOKEN);
    removeItem(LOCALSTORAGE_KEY.EXPIRED_TIME);
    if (window.location.origin + "/login" !== window.location.href) {
      window.location.href = "/login";
    }
  };

  return {
    register,
    login,
    getSession,
    refreshToken,
    logout
  };
};

export default useAmazonCognito;
