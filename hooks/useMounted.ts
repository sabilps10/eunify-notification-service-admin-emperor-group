import { useState, useEffect } from "react";

const useMounted = () => {
  const [isMounted, setMounted] = useState(false);

  useEffect(() => {
    let isMounted = false;
    setMounted(!isMounted);
    return () => {
      isMounted = true;
    };
  }, []);

  return {
    isMounted
  };
};

export default useMounted;
