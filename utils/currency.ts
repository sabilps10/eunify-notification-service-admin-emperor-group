import LocaleCurrency from "locale-currency";

// Overries ts augmentating getLocales function
declare module "locale-currency" {
  export function getLocales(currency: string): string;
}

export const formatCurrency = (currency: string, value: number) => {
  let newCurrency = currency;
  // TODO: temporary will remove
  if (currency === "string") {
    newCurrency = "USD";
  }

  const locale = LocaleCurrency.getLocales(newCurrency);

  return new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: newCurrency,
    currencyDisplay: "code",
    minimumFractionDigits: 0
  })
    .format(value)
    .replace(newCurrency, "")
    .trim();
};
