import { CognitoUserPool, CookieStorage } from "amazon-cognito-identity-js";

const poolData = {
  UserPoolId: process.env.NEXT_PUBLIC_COGNITO_USER_POOL_ID || "",
  ClientId: process.env.NEXT_PUBLIC_COGNITO_CLIENT_ID || "",
  Storage: new CookieStorage({
    domain: process.env.NEXT_PUBLIC_DOMAIN ? process.env.NEXT_PUBLIC_DOMAIN : "http://localhost:3000",
    secure: true,
  })
};

export default new CognitoUserPool(poolData);
