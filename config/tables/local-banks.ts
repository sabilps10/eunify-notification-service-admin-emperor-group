export const LOCAL_BANK_HEADER = [
  {
    id: "no",
    numeric: true,
    disablePadding: false,
    label: "No.",
    isSort: true
  },
  {
    id: "bankCode",
    numeric: true,
    disablePadding: false,
    label: "Bank Code",
    isSort: true
  },
  {
    id: "bankNameSys",
    numeric: false,
    disablePadding: false,
    label: "System Name",
    isSort: true
  },
  {
    id: "bankNameEn",
    numeric: false,
    disablePadding: false,
    label: "Eng Display Name",
    isSort: true
  },
  {
    id: "bankNameTc",
    numeric: false,
    disablePadding: false,
    label: "Trad Ch Display Name",
    isSort: true
  },
  {
    id: "bankNameSc",
    numeric: false,
    disablePadding: false,
    label: "Simp Ch Display Name",
    isSort: true
  },
  {
    id: "edda",
    numeric: false,
    disablePadding: false,
    label: "eDDA",
    isSort: true
  },
  {
    id: "isActive",
    numeric: false,
    disablePadding: false,
    label: "Status",
    isSort: true
  },
  {
    id: "action",
    numeric: false,
    disablePadding: false,
    label: "Action",
    key_value: "bankNameSys",
    isSort: true,
    idValue: "bankId"
  }
];

export const LOCAL_BANK_ACTION = [{ name: "add", action: { type: "link", value: "/add" } }];
