import { FC, useCallback, useEffect, useState } from "react";
import TableList from "@/components/TableList";
import {
  REPORT_ACTION,
  REPORT_HEADER,
  REPORT_EXPORT_HEADING
} from "@/config/tables/report-record";
import { useMutation, useQuery } from "react-query";
import { cancelNotificationRequest, getReportQuery, rescheduleNotificationRequest, retryNotificationRequest } from "@/services/common-services";
import moment from "moment";
import { Card, Stack } from "@mui/material";
import toast from "react-hot-toast";

export const REPORT_FILTER: any = [
  { id: "applicationDate", name: "applicationDate", type: "time-range" },
  { id: "applicationCode", name: "applicationCode", type: "text", options: [] },
  { id: "type", name: "type", type: "dropdown", options: [{value: "", label: "All Type"}, {value: "sms", label: "SMS"}, {value: "email", label: "Email"}, {value: "pushnotif", label: "Push Notification"}] },
  { id: "status", name: "status", type: "dropdown", options: [{value: "", label: "All Status"}, {value: "CREATED", label: "Created"}, {value: "PENDING", label: "Pending"}, {value: "PROCESSING", label: "Processing"}, {value: "SUCCESS", label: "Success"}, {value: "FAILED", label: "Failed"}, {value: "CANCELLED", label: "Cancelled"}] }
];

const defaultFilterData = { startDate: moment().format("Y-MM-DD"), endDate:  moment().format("Y-MM-DD"), applicationCode: null, type: null, status: null };

const ReportListing: FC = () => {
  const [, updateState] = useState<any>();
  const forceUpdate = useCallback(() => updateState({}), []);
  const [rowActionDisabled, setRowActionDisabled] = useState<boolean>(false)
  const [retryPayload, setRetryPayload] = useState<any>({id: null})

  const [filterData, setFilterData] = useState({
    startDate: moment().format("Y-MM-DD"),
    endDate: moment().format("Y-MM-DD"),
    applicationCode: null,
    type: null,
    status: null
  });

  const getReportData = useQuery(
    ["get-report-query", filterData],
    async () => {
      const { response } = await getReportQuery(filterData);
      return response;
    },
    { keepPreviousData: true }
  );

  const { mutate: retryReport } = useMutation(({ id }: {id: any}) =>
    retryNotificationRequest({id: id})
  );

  const { mutate: cancelReport } = useMutation(({ id }: {id: any}) =>
    cancelNotificationRequest({id: id})
  );

  const { mutate: rescheduleReport } = useMutation(({ id, datetime }: {id: any, datetime: any}) =>
    rescheduleNotificationRequest({id: id, scheduledDateTime: datetime})
  );

  const { data: reportData, isLoading, isFetching, refetch } = getReportData;

  const exportToFileObject = { heading: REPORT_EXPORT_HEADING };

  const handleDoFilterData = (values: any) => {
    let filter: any = filterData;
    Object.keys(values).forEach(function (key) {
      if (key == "applicationDate_start" || key == "applicationDate_end") {
        const splitString = key.split("_");
        if (splitString[1] == "start") {
          filter["startDate"] = values.applicationDate_start
            ? moment(values.applicationDate_start).format("Y-MM-DD")
            : null;
        }
        if (splitString[1] == "end") {
          filter["endDate"] = values.applicationDate_end
            ? moment(values.applicationDate_end).format("Y-MM-DD")
            : null;
        }
      } else {
        filter[key] = values[key] ? values[key] : null;
      }
    });
    setFilterData({ ...filterData, ...filter });
  };

  const rowAction = (type: string, value: any) => {
    setRowActionDisabled(true)
    if(type == "retry"){
      retryReport(
        { id: value},
        {
          onSuccess: (data) => {
            toast.success("Retried selected request");
            refetch()
            setRowActionDisabled(false)
          }
        }
      );
    }
    if(type == "cancel"){
      cancelReport(
        { id: value},
        {
          onSuccess: (data) => {
            toast.success("Cancelled selected request");
            refetch()
            setRowActionDisabled(false)
          }
        }
      );
    }
    if(type == "reschedule"){
      rescheduleReport(
        { id: value.id, datetime: moment(value.datetime).format("Y-MM-DD HH:mm:ss")},
        {
          onSuccess: (data) => {
            toast.success("Rescheduled selected request");
            refetch()
            setRowActionDisabled(false)
          }
        }
      );
    }
  }
  return (
    <Stack>
      <Stack direction="row" spacing={2} sx={{marginBottom:2}}>
        <Card variant="outlined" sx={{width: "100%", padding: 2}}>Request Count: <b>{reportData?.request_count}</b></Card>
        <Card variant="outlined" sx={{width: "100%", padding: 2}}>Success Count: <b>{reportData?.success_count}</b></Card>
        <Card variant="outlined" sx={{width: "100%", padding: 2}}>Success Rate: <b>{reportData?.success_rate}</b></Card>
        <Card variant="outlined" sx={{width: "100%", padding: 2}}>Failed Count: <b>{reportData?.failed_count}</b></Card>
      </Stack>
      <TableList
        header={REPORT_HEADER}
        filter={REPORT_FILTER}
        action={REPORT_ACTION}
        data={reportData?.notification_response}
        isLoading={isFetching}
        onFilterData={(values: any) => handleDoFilterData(values)}
        exportDataObject={exportToFileObject}
        rowActionCallback={rowAction}
        rowActionDisabled={rowActionDisabled}
      />
    </Stack>
  );
};

export default ReportListing;
