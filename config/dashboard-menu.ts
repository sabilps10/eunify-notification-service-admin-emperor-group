export const ListMenu = [
  {
    label: "Transactions Management",
    name: "transactions",
    children: [
      {
        number: 1,
        label: "Deposit Management",
        name: "deposit",
        link: "/deposit/management"
      },
      {
        number: 1,
        label: "Withdrawal Management",
        name: "withdrawal",
        link: "/withdrawal/management"
      },
      {
        number: 1,
        label: "Transaction Records",
        name: "transaction",
        link: "/transaction/management"
      }
    ]
  },
  {
    label: "Payment Channel Settings",
    name: "payment-channel",
    children: [
      {
        number: 1,
        label: "Deposit Channel",
        name: "deposit-channel",
        link: "/deposit-channel/management"
      },
      {
        number: 1,
        label: "Withdrawal Channel",
        name: "withdrawal-channel",
        link: "/withdrawal-channel/management"
      },
      {
        number: 1,
        label: "Beneficiary Accounts",
        name: "beneficiary-accounts",
        link: "/beneficiary-accounts/management"
      },
      {
        number: 1,
        label: "Exchange Rates",
        name: "exchange-rates",
        link: "/exchange-rates/management"
      },
      {
        number: 1,
        label: "Blacklist",
        name: "blacklist",
        link: "/blacklist/management"
      }
    ]
  },
  {
    label: "CCBA Bank Records",
    name: "ccba-records",
    link: "/ccba-records/management"
  },
  {
    label: "Payment Management",
    name: "payment",
    link: "/payment/management?tab=Reject+Deposit"
  },
  {
    label: "Exceptions Management",
    name: "exceptions",
    link: "/exceptions/management"
  },
  {
    label: "Bank & Credit Card Record",
    name: "bank-management",
    link: "/bank-management/management"
  },
  {
    label: "eDDA Record",
    name: "edda-record",
    link: "/edda-record/management"
  },
  {
    label: "Reports",
    name: "reports",
    link: "/reports/management"
  },
  {
    label: "System Setting",
    name: "system-setting",
    children: [
      {
        number: 1,
        label: "User Management",
        name: "user-management",
        link: "/user-management/management"
      },
      {
        number: 1,
        label: "Role Settings",
        name: "role-settings",
        link: "/role-settings/management"
      },
      {
        number: 1,
        label: "Code & Values",
        name: "code-values",
        children: [
          {
            number: 2,
            label: "List of Key Term Naming",
            name: "list-keys",
            link: "/list-keys/management"
          },
          {
            number: 2,
            label: "Key Term Naming for PRC Province",
            name: "term-naming-province",
            link: "/term-naming-province/management"
          },
          {
            number: 2,
            label: "Key Term Naming for PRC Region/Distric",
            name: "term-naming-district",
            link: "/term-naming-district/management"
          },
          {
            number: 2,
            label: "Key Term Naming for PRC City",
            name: "term-naming-city",
            link: "/term-naming-city/management"
          },
          {
            number: 2,
            label: "Manual Adjustments Types",
            name: "manual-adjustment-types",
            link: "/manual-adjustment-types/management"
          },
          {
            number: 2,
            label: "Local Bank List eDDA Mapping",
            name: "local-bank-edda",
            link: "/local-bank-edda/management"
          }
        ]
      },
      {
        number: 1,
        label: "Dummy Account List",
        name: "dummy-account",
        link: "/dummy-account/management"
      },
      {
        number: 1,
        label: "Bank & CC Registration Limit",
        name: "bank-account-limit",
        link: "/bank-account-limit/management"
      },
      {
        number: 1,
        label: "Notification Management",
        name: "notification-management",
        link: "/notification/management"
      }
    ]
  },
  {
    label: "Audit Log",
    name: "audit-log",
    link: "/audit-log/management"
  }
];
