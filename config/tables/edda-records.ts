export const EDDA_BANK_HEADER = [
  {
    id: "entityId",
    numeric: true,
    disablePadding: false,
    label: "Entity",
    isSort: true
  },
  {
    id: "tradingAccountNo",
    numeric: false,
    disablePadding: false,
    label: "Trading Account No.",
    isSort: true
  },
  {
    id: "tradingAccountName",
    numeric: false,
    disablePadding: false,
    label: "Trading Account Name",
    isSort: true
  },
  {
    id: "eddaRecipientBank",
    numeric: false,
    disablePadding: false,
    label: "eDDA Recipient Bank",
    isSort: true
  },
  {
    id: "bankCode",
    numeric: false,
    disablePadding: false,
    label: "Bank Code",
    isSort: true
  },
  {
    id: "bankName",
    numeric: false,
    disablePadding: false,
    label: "Bank Name",
    isSort: true
  },
  {
    id: "bankAccountNo",
    numeric: false,
    disablePadding: false,
    label: "Bank Account No.",
    isSort: true
  },
  {
    id: "submitDate",
    numeric: false,
    disablePadding: false,
    label: "Application Date",
    isSort: true
  },
  {
    id: "lastModifiedDate",
    numeric: false,
    disablePadding: false,
    label: "Last Update Date",
    isSort: true
  },
  {
    id: "status",
    numeric: false,
    disablePadding: false,
    label: "Status",
    isSort: true
  },
  {
    id: "rejectCode",
    numeric: false,
    disablePadding: false,
    label: "Reject Code",
    isSort: true
  },
  {
    id: "rejectReasonInfo",
    numeric: false,
    disablePadding: false,
    label: "Reject Info",
    isSort: true
  }
];

export const EDDA_BANK_ACTION = [{ name: "export", action: { type: "export_csv", value: "" } }];

export const EDDA_BANK_EXPORT_HEADING = [
  { name: "entityId", label: "Entity", width: 20, position: 1 },
  { name: "tradingAccountNo", label: "Trading Account No.", width: 20, position: 2 },
  { name: "tradingAccountName", label: "Trading Account Name", width: 20, position: 3 },
  { name: "eddaRecipientBank", label: "eDDA Recipient Bank", width: 20, position: 4 },
  { name: "bankCode", label: "Bank Code", width: 20, position: 5 },
  { name: "bankName", label: "Bank Name", width: 20, position: 6 },
  { name: "bankAccountNo", label: "Bank Acc No.", width: 20, position: 7 },
  { name: "submitDate", label: "Application Date", width: 20, position: 8 },
  { name: "lastModifiedDate", label: "Last Update Date", width: 20, position: 9 },
  {
    name: "status",
    label: "Status",
    type: "custom-value",
    custom_values: [
      { value: 0, label: "Pending" },
      { value: 1, label: "Success" },
      { value: 2, label: "Failed" }
    ],
    width: 20,
    position: 10
  },
  { name: "rejectCode", label: "Reject Code", width: 20, position: 11 },
  { name: "rejectReasonInfo", label: "Reject Info", width: 20, position: 12 }
];
