import { object, string } from "zod";

export const LocalBankFields = [
  { name: "status", type: "select", disabled: false },
  { name: "bank_code", type: "text", disabled: false },
  { name: "system_display_name", type: "text", disabled: false },
  { name: "english_display_name", type: "text", disabled: false },
  { name: "trad_chi_display_name", type: "text", disabled: false },
  { name: "simp_chi_display_name", type: "text", disabled: false },
  {
    name: "edda_bank",
    type: "radio",
    options: [
      { name: "ccba", label: "CCBA" },
      { name: "hsbc", label: "HSBC" }
    ],
    disabled: false
  }
];

export const LocalBankSchema = object({
  status: string().min(1, { message: "Status is required" }),
  bank_code: string().min(1, { message: "Bank Code is required" }),
  system_display_name: string().min(1, { message: "Sytem Display Name is required" }),
  english_display_name: string().min(1, { message: "Eng. Display Name is required" }),
  trad_chi_display_name: string().min(1, { message: "Trad. Ch Display Name is required" }),
  simp_chi_display_name: string().min(1, { message: "Simp. Ch Display Name is required" })
});
