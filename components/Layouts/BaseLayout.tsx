import { FC, PropsWithChildren } from "react";

import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import AppBarComponent from "./AppBar";
import DrawerComponent from "./Drawer";
import { useRouter } from "next/router";
import useMounted from "@/hooks/useMounted";
import { Toaster } from "react-hot-toast";

const BaseLayout: FC<PropsWithChildren> = ({ children }) => {
  const { pathname, ...props } = useRouter();
  const isLoginPageOr404 = pathname == "/login" || props.route == "/404";
  const { isMounted } = useMounted();

  if (!isMounted) return <></>;
  return (
    <Box sx={{ display: "flex" }}>
      <Toaster />
      <AppBarComponent isLoginPageOr404={isLoginPageOr404} />
      {isLoginPageOr404 == false && (
        <DrawerComponent />
      )}
      <Box
        component="main"
        sx={{
          backgroundColor: (theme) => theme.palette.grey[100],
          flexGrow: 1,
          height: "100vh",
          overflow: "auto",
          pt: 3
        }}
      >
        <Toolbar />
        <Container maxWidth={false}>
          <Grid container>{children}</Grid>
        </Container>
      </Box>
    </Box>
  );
};

export default BaseLayout;
