export const APPLICABLE_GROUP_HEADER = [
  {
    id: "group",
    label: "Group",
    isSort: true
  },
  {
    id: "amount",
    label: "Amount",
    isSort: true
  },
  {
    id: "type",
    label: "Type",
    isSort: true
  }
];

export const AVAILABLE_OPERATION_TIME_HEADER = [
  {
    id: "selected",
    label: ""
  },
  {
    id: "day",
    label: ""
  },
  {
    id: "from",
    label: "From"
  },
  {
    id: "to",
    label: "To"
  },
  {
    id: "break_time_from",
    label: "Break Time From"
  },
  {
    id: "break_time_to",
    label: "Break Time To"
  }
];
