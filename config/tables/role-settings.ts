export const ROLE_SETTINGS_ACTION = [{ name: "add", action: { type: "link", value: "/add" } }];
export const ROLE_SETTINGS_HEADER = [
  {
    id: "no",
    numeric: false,
    disablePadding: false,
    label: "No",
    isSort: true
  },
  {
    id: "roleNameEn",
    numeric: false,
    disablePadding: false,
    label: "Role Name",
    isSort: true
  },
  {
    id: "action",
    numeric: false,
    disablePadding: false,
    label: "Action",
    key_value: "roleNameEn",
    isSort: true,
    idValue: "roleId"
  }
];
