import useFormContent from "@/features/dashboard/hooks/useFormContent";
import { FC, useCallback, useEffect, useState } from "react";
import DetailContentForm from "../../DetailContentForm";

import useFormData from "@/hooks/useFormData";

export const ReportFields: any = [
  { id: "applicationCode", name: "application_code", type: "text", disabled: true },
  { id: "type", name: "type", type: "text", disabled: true},
  { id: "status", name: "status", type: "text", disabled: true},
  { id: "scheduleType", name: "schedule_type", type: "text", disabled: true},
  { id: "createdBy", name: "created_by", type: "text", disabled: true},
  { id: "errorCode", name: "error_code", type: "text", disabled: true},
  { id: "errorMessage", name: "error_message", type: "text", disabled: true},
  { id: "createdDate", name: "created_date", type: "text", disabled: true},
  { id: "lastModifiedBy", name: "last_modified_by", type: "text", disabled: true},
  { id: "lastModifiedDate", name: "last_modified_date", type: "text", disabled: true},
  { id: "content", name: "data_content", type: "text", value_type: "textarea", disabled: true},
];

const ReportForm: FC = () => {
  const { formFields, setFormValue } = useFormContent();

  const { data: formData, setFormData } = useFormData();
  const [initialData, setInitialData] = useState(null);

  useEffect(() => {
    if (!formFields) {
      setFormValue("fields", ReportFields);
    }
    if (formData) {
      let current: any = {};
      ReportFields.map((field: any) => {
        if (
          formData[field.id] || formData?.errorInfo || field.id == "content"
        ) {
          let value = null;
          if(field.id == "errorCode" || field.id == "errorMessage"){
            value = formData?.errorInfo[field.id].toString();
          }else if(field.id == "content") {
            if(formData.type.toLowerCase() == "email"){
                value = JSON.stringify(formData.emailContent)
            }
            if(formData.type.toLowerCase() == "sms"){
                value = JSON.stringify(formData.smsContent)
            }
            if(formData.type.toLowerCase() == "push_notification"){
                value = JSON.stringify(formData.pushNotificationContent)
            }
          }else{
            value = formData[field.id].toString()
          }
          Object.assign(current, { [field.name]: value });
        }
      });
      setInitialData(current);
    }
  }, []);

  return (
    <DetailContentForm
      formData={formData}
      initialData={initialData}
      fields={formFields}
      validations={{}}
      onCancel={() => setFormData(null)}
    />
  );
};

export default ReportForm;
