import { FC } from "react";

import { Container, Stack } from "@mui/material";
import DashboardTitle from "./components/DashboardTitle";
import DashboardContent from "./components/DashboardContent";

const DashboardList: FC = () => {
  return (
    <Container maxWidth={false}>
      <DashboardTitle />
      <DashboardContent />
    </Container>
  );
};

export default DashboardList;
