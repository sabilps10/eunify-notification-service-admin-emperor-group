import { createTheme } from "@mui/material/styles";

import componentsTheme from "./components";
import colorsTheme from "./colors";
import typographyTheme from "./typography";

const theme = createTheme({
  components: componentsTheme.components,
  palette: colorsTheme.palette,
  typography: typographyTheme.typography
});

export default theme;
